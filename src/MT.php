<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiException;

/**
 * Lingo24 Machine Translation API client. Provides the base functions for interacting with the MT API.
 */
class MT extends AbstractUserKeyClient
{
    /**
     * @var string $BASE_URL The base MT API url.
     */
    private static $BASE_URL = 'https://api.lingo24.com/mt/v1/';

    /**
     * @var string $SOURCES_ENDPOINT End point for source language requests.
     */
    private static $SOURCES_ENDPOINT = 'sourcelangs';

    /**
     * @var string $TARGETS_ENDPOINT End point for target language requests.
     */
    private static $TARGETS_ENDPOINT = 'targetlangs';

    /**
     * @var string $TRANSLATE_ENDPOINT End point for translation requests.
     */
    private static $TRANSLATE_ENDPOINT = 'translate';

    /**
     * @var string $DETECT_ENDPOINT End point for language detection requests.
     */
    private static $DETECT_ENDPOINT = 'langid';

    /**
     * @var string $FALSE The false success status returned from the API when there is a problem.
     */
    private static $FALSE = 'false';

    /**
     * Constructor for the API client, the 3 Scale user key is required.
     *
     * @param string $userKey       The 3 Scale user key for thee Lingo24 API.
     * @param String $customBaseUrl A custom base URL to use.
     */
    public function __construct($userKey, $customBaseUrl = null)
    {
        $this->setUserKey($userKey);
        $this->setCustomBaseUrl($customBaseUrl);
    }

    /**
     * Returns a list of potential source languages, optionally filtered by languages which are able to be translated
     * into a target. This function is useful for building up a drop down of possible language combinations.
     *
     * @param string|null $target Optional target language, filters the returned list.
     *
     * @return array Available languages, keyed by language code.
     */
    public function getSources($target = null)
    {
        $params = array();
        if ($target) {
            $params['target'] = $target;
        }

        $response = $this->get($this->getBaseUrl() . self::$SOURCES_ENDPOINT, $params);

        if (!isset($response->source_langs) || !isset($response->success) || $response->success == self::$FALSE) {
            throw new Lingo24ApiException('Could not retrieve source languages from API.');
        }

        $languages = array();

        foreach ($response->source_langs as $lang) {
            $languages[$lang[0]] = $lang[1];
        }

        return $languages;
    }

    /**
     * Returns a list of potential target languages, optionally filtered by languages which are able to be translated
     * from a source. This function is useful for building up a drop down of possible language combinations.
     *
     * @param string|null $target Optional source language, filters the returned list.
     *
     * @return array Available languages, keyed by language code.
     */
    public function getTargets($source = null)
    {
        $params = array();
        if ($source) {
            $params['source'] = $source;
        }

        $response = $this->get($this->getBaseUrl() . self::$TARGETS_ENDPOINT, $params);

        if (!isset($response->target_langs) || !isset($response->success) || $response->success == self::$FALSE) {
            throw new Lingo24ApiException('Could not retrieve target languages from API.');
        }

        $languages = array();
        foreach ($response->target_langs as $lang) {
            $languages[$lang[0]] = $lang[1];
        }

        return $languages;
    }

    /**
     * Translate a string from the source language to the target, or alternatively translate a string using a known
     * specific translation engine.
     *
     * @param mixed ... The function optionally takes either 3 arguments: source language code, target language code and
     *                  the input text to be translated; or 2 arguments: the translation engine id and the input text to
     *                  be translated.
     *
     * @return string The translated text.
     */
    public function translate()
    {
        $params = array();

        $args = func_get_args();
        if (func_num_args() == 2) {
            $params = array('id' => $args[0], 'q' => $args[1]);
        } else {
            $params = array('source' => $args[0], 'target' => $args[1], 'q' => $args[2]);
        }

        $response = $this->post(self::$BASE_URL . self::$TRANSLATE_ENDPOINT, $params);

        if (isset($response->success) &&  $response->success == self::$FALSE) {
            throw new Lingo24ApiException('Unable to translate input: ' . implode(',', $response->errors));
        }

        return $response->translation;
    }

    /**
     * Detect the language of a piece of content.
     *
     * @param string $content The content to detect the language of.
     *
     * @return string The detected language.
     */
    public function detectLanguage($content)
    {
        $params = array('q' => $content);

        $response = $this->post($this->getBaseUrl() . self::$DETECT_ENDPOINT, $params);

        if (isset($response->success) &&  $response->success == self::$FALSE) {
            throw new Lingo24ApiException('Unable to translate input.');
        }

        return $response->lang;
    }

    /**
     * Return the API's base url.
     *
     * @return string The base url.
     */
    public function getBaseUrl()
    {
        if ($this->getCustomBaseUrl() != null) {
            return $this->getCustomBaseUrl();
        }

        return self::$BASE_URL;
    }
}
