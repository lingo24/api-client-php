<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiAuthorisationException;
use Lingo24\API\Exception\Lingo24ApiException;
use Lingo24\API\Exception\Lingo24ApiNotAllowedException;
use Lingo24\API\Exception\Lingo24ApiNotFoundException;
use Lingo24\API\Model\Domain;
use Lingo24\API\Model\File;
use Lingo24\API\Model\FileType;
use Lingo24\API\Model\Job;
use Lingo24\API\Model\JobMetric;
use Lingo24\API\Model\JobStatus;
use Lingo24\API\Model\JobPrice;
use Lingo24\API\Model\ProjectCharge;
use Lingo24\API\Model\TranslationLocale;
use Lingo24\API\Model\OAuth2;
use Lingo24\API\Model\Project;
use Lingo24\API\Model\ProjectPrice;
use Lingo24\API\Model\ProjectStatus;
use Lingo24\API\Model\Service;

/**
 * Lingo24 Machine Translation API client. Provides the base functions for interacting with the MT API.
 */
class Docs extends AbstractOAuth2Client
{
    /**
     * @var string $BASE_URL_DEMO The base Business Documents API demo environment url.
     */
    public static $BASE_URL_DEMO = 'https://api-demo.lingo24.com/docs/v1/';

    /**
     * @var string $BASE_URL The base Business Documents API url.
     */
    public static $BASE_URL_LIVE = 'https://api.lingo24.com/docs/v1/';

    /**
     * @var string $AUTH_URL_DEMO The Ease demo environment OAuth2 authorisation url.
     */
    public static $AUTH_URL_DEMO = 'https://ease-demo.lingo24.com/ui/oauth2/authorize';

    /**
     * @var string $AUTH_URL_LIVE The Ease OAuth2 authorisation url.
     */
    public static $AUTH_URL_LIVE = 'https://ease.lingo24.com/ui/oauth2/authorize';

    /**
     * @var string $SERVICE_ENDPOINT End point for service requests.
     */
    public static $SERVICE_ENDPOINT = 'services';

    /**
     * @var string $LOCALE_ENDPOINT End point for locale requests.
     */
    public static $LOCALE_ENDPOINT = 'locales';

    /**
     * @var string $DOMAIN_ENDPOINT End point for domain requests.
     */
    public static $DOMAIN_ENDPOINT = 'domains';

    /**
     * @var string $PROJECT_ENDPOINT End point for project requests.
     */
    public static $PROJECT_ENDPOINT = 'projects';

    /**
     * @var string $JOB_ENDPOINT End point for job requests.
     */
    public static $JOB_ENDPOINT = 'jobs';

    /**
     * @var string $PRICE_ENDPOINT End point for price requests.
     */
    public static $PRICE_ENDPOINT = 'price';

    /**
     * @var string $FILE_ENDPOINT End point for file requests.
     */
    public static $FILE_ENDPOINT = 'files';

    /**
     * @var string $JOB_METRICS_ENDPOINT End point for job metrics requests.
     */
    public static $JOB_METRICS_ENDPOINT = 'metrics';

    /**
     * @var string $FILE_CONTENT_ENDPOINT End point for file content requests.
     */
    public static $FILE_CONTENT_ENDPOINT = 'content';

    /**
     * @var string $PROJECT_CHARGES_ENDPOINT End point for project charges requests.
     */
    public static $PROJECT_CHARGES_ENDPOINT = 'charges';

    /**
     * @var string $TAGS_ENDPOINT End point for tags requests.
     */
    public static $TAGS_ENDPOINT = 'tags';

    /**
     * @var array Local cache of services.
     */
    private $services;

    /**
     * @var array Local cache of locales.
     */
    private $locales;

    /**
     * @var array Local cache of domains.
     */
    private $domains;

    /**
     * @var array Local cache of tags.
     */
    private $tags;

    /**
     * Fetch the list of available services from the API.
     *
     * @return array An array of services.
     */
    public function getServices()
    {
        if ($this->services == null) {
            $results = $this->getJson($this->getBaseUrl() . self::$SERVICE_ENDPOINT);

            if ($results->page->totalElements > count($results->content)) {
                $results = $this->getJson(
                    $this->getBaseUrl() . self::$SERVICE_ENDPOINT,
                    array('size' => $results->page->totalElements)
                );
            }

            foreach ($results->content as $result) {
                $this->services[] = new Service($result);
            }
        }

        return $this->services;
    }

    /**
     * Return a specific service.
     *
     * @param int $serviceId The id of the service to return.
     *
     * @return Service The service, or null if it cannot be found.
     */
    public function getService($serviceId)
    {
        foreach ($this->getServices() as $service) {
            if ($service->getId() == $serviceId) {
                return $service;
            }
        }

        return null;
    }

    /**
     * Fetch the list of available locales from the API.
     *
     * @return array An array of services.
     */
    public function getLocales()
    {
        if ($this->locales == null) {
            $results = $this->getJson($this->getBaseUrl() . self::$LOCALE_ENDPOINT);

            if ($results->page->totalElements > count($results->content)) {
                $results = $this->getJson(
                    $this->getBaseUrl() . self::$LOCALE_ENDPOINT,
                    array('size' => $results->page->totalElements)
                );
            }

            foreach ($results->content as $result) {
                $this->locales[] = new TranslationLocale($result);
            }

            usort($this->locales, 'Lingo24\API\Model\TranslationLocale::cmp');
        }

        return $this->locales;
    }

    /**
     * Return a specific locale.
     *
     * @param int|string $localeOrId The id of the locale to return.
     *
     * @return TranslationLocale The locale, or null if it cannot be found.
     */
    public function getLocale($localeOrId)
    {
        if (preg_match('/([a-z]+)_([A-Z]+)/', $localeOrId, $matches)) {
            foreach ($this->getLocales() as $locale) {
                if ($locale->getLanguage() == $matches[1]
                        && $locale->getCountry() == $matches[2]) {
                    return $locale;
                }
            }
        } else {
            foreach ($this->getLocales() as $locale) {
                if ($locale->getId() == $localeOrId) {
                    return $locale;
                }
            }
        }

        return null;
    }

    /**
     * Fetch the list of available domains from the API.
     *
     * @return array An array of domains.
     */
    public function getDomains()
    {
        if ($this->domains == null) {
            $results = $this->getJson($this->getBaseUrl() . self::$DOMAIN_ENDPOINT);

            if ($results->page->totalElements > count($results->content)) {
                $results = $this->getJson(
                    $this->getBaseUrl() . self::$DOMAIN_ENDPOINT,
                    array('size' => $results->page->totalElements)
                );
            }

            foreach ($results->content as $result) {
                $this->domains[] = new Domain($result);
            }
        }

        return $this->domains;
    }

    /**
     * Return a specific domain.
     *
     * @param int $domainId The id of the domain to return.
     *
     * @return Domain The domain, or null if it cannot be found.
     */
    public function getDomain($domainId)
    {
        foreach ($this->getDomains() as $domain) {
            if ($domain->getId() == $domainId) {
                return $domain;
            }
        }

        return null;
    }

    /**
     * Fetch a list of projects currently available to the logged in user.
     *
     * @param int    $size          The number of jobs to return, defaults to 10.
     * @param int    $page          The paging offset, defaults to 0.
     * @param string $sortField     The field to sort by, defaults to created date.
     * @param string $sortDirection The direction to sort on, defaults to desc.
     *
     * @return array An array of projects.
     */
    public function getProjects($size = 10, $page = 0, $sortField = 'created', $sortDirection = 'desc')
    {
        $results = $this->getJson(
            $this->getBaseUrl() . self::$PROJECT_ENDPOINT,
            array('size' => $size, 'page' => $page, 'sort' => "$sortField,$sortDirection")
        );

        $projects = array();
        foreach ($results->content as $result) {
            $projects[] = new Project($result);
        }

        return $projects;
    }

    /**
     * Fetch a single project by id.
     *
     * @param int $projectId A project id.
     *
     * @return Project The project.
     */
    public function getProject($projectId)
    {
        try {
            return $this->getResource(
                $this->getBaseUrl() . self::$PROJECT_ENDPOINT . "/$projectId",
                '\Lingo24\API\Model\Project'
            );
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Create a new Lingo24 project.
     *
     * @param Project|string $projectOrName An existing project or a project name.
     *
     * @return Project The created project.
     */
    public function createProject($projectOrName)
    {
        $payload = array();
        if ($projectOrName instanceof Project) {
            $payload['name'] = $projectOrName->getName();
            $payload['tags'] = $projectOrName->getTags();
        } else {
            $payload['name'] = $projectOrName;
        }

        $result = $this->postJson($this->getBaseUrl() . self::$PROJECT_ENDPOINT, $payload);

        return new Project($result);
    }

    /**
     * Update an existing Lingo24 project.
     *
     * @param Project $project An existing project.
     *
     * @return Project The updated project.
     */
    public function updateProject(Project $project)
    {
        if ($project->getId() == null) {
            throw new Lingo24ApiException('Cannot update a project with no id, create the project first.');
        }

        $payload = array('id'            => $project->getId(),
                         'name'          => $project->getName(),
                         'projectStatus' => $project->getStatus()->getValue());

        $result = $this->putJson($project->getSelf(), $payload);

        return new Project($result);
    }

    /**
     * Quote an existing Lingo24 project. Changes the project status to QUOTED, to be held by the API for 15 days.
     *
     * @param Project $project An existing project.
     *
     * @return Project The updated project.
     */
    public function quoteProject(Project $project)
    {
        if ($project->getId() == null) {
            throw new Lingo24ApiException("Cannot quote a project with no id, create the project first.");
        }

        if (!$project->getStatus()->equals(ProjectStatus::CREATED)) {
            throw new Lingo24ApiException(
                'Cannot quote a project which does not have a status of CREATED.'
            );
        }

        $project->setStatus(ProjectStatus::QUOTED);

        return $this->updateProject($project);
    }

    /**
     * Confirm an existing Lingo24 project.
     *
     * @param Project $project An existing project.
     *
     * @return Project The updated project.
     */
    public function confirmProject(Project $project)
    {
        if ($project->getId() == null) {
            throw new Lingo24ApiException("Cannot confirm a project with no id, create the project first.");
        }

        if (!$project->getStatus()->equals(ProjectStatus::CREATED)
                && !$project->getStatus()->equals(ProjectStatus::QUOTED)) {
            throw new Lingo24ApiException(
                'Cannot confirm a project which does not have a status of CREATED or QUOTED.'
            );
        }

        $project->setStatus(ProjectStatus::IN_PROGRESS);

        return $this->updateProject($project);
    }

    /**
     * Cancel an existing Lingo24 project.
     *
     * @param Project $project An existing project.
     *
     * @return Project The updated project.
     */
    public function cancelProject(Project $project)
    {
        if ($project->getId() == null) {
            throw new Lingo24ApiException('Cannot cancel a project with no id.');
        }

        $project->setStatus(ProjectStatus::CANCELLED);

        return $this->updateProject($project);
    }

    /**
     * Delete an existing Lingo24 project.
     *
     * @param Project $project The project to delete.
     */
    public function deleteProject(Project $project)
    {
        if (!$project->getStatus()->equals(ProjectStatus::CREATED)
                && !$project->getStatus()->equals(ProjectStatus::QUOTED)) {
            throw new Lingo24ApiException('The project is confirmed, you cannot delete it.');
        }

        $this->delete($project->getSelf());
    }

    /**
     * Fetch the price for a project, backwards compatible version with function which previously added job prices to
     * calculate the project price.
     *
     * @param Project $project The project to calculate the price for.
     *
     * @return array|NULL The total price and currency or null if the price is unavailable.
     */
    public function calculateProjectPrice(Project $project)
    {
        $price = $this->getProjectPrice($project);
        if ($price == null) {
            return null;
        }

        return array('price' => $price->getTotalWVatWDiscount(), 'currency' => $price->getCurrency());
    }

    /**
     * Fetch a price for a project.
     *
     * @param Project|string $projectOrHref Project object for a href for the price.
     *
     * @return ProjectPrice The project price.
     */
    public function getProjectPrice($projectOrHref)
    {
        if ($projectOrHref instanceof Project) {
            $projectOrHref = $projectOrHref->getLink(Project::PRICE_REL);
        }

        try {
            $result = $this->getJson($projectOrHref);
            $projectPrice = null;
            if ($result != null) {
                $projectPrice = new ProjectPrice($result);
            }

            return $projectPrice;
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Fetch a list of project charges for a given project.
     *
     * @param int|Project $project A project id or project.
     * @param int         $size    The number of project charges to return, defaults to 10.
     * @param int         $page    The page number, defaults to 0.
     *
     * @return array An array of project charges.
     */
    public function getProjectCharges($project, $size = 10, $page = 0)
    {
        if (is_numeric($project)) {
            $project = $this->getProject($project);
        }

        $results = $this->getJson(
            $project->getLink(Project::CHARGES_REL),
            array('size' => $size, 'page' => $page)
        );

        $projectCharges = array();
        foreach ($results->content as $result) {
            $projectCharges[] = new ProjectCharge($result);
        }

        return $projectCharges;
    }

    /**
     * Fetch a single file by id or job/type.
     *
     * @param int|Job $fileIdOrJob  The id of the file or the job to fetch a file for.
     * @param string  $type         The file type, source or target.
     *
     * @return File A file object.
     */
    public function getFile($fileIdOrJob, $type = null)
    {
        $href = null;

        if (is_numeric($fileIdOrJob)) {
            $href = $this->getBaseUrl() . self::$FILE_ENDPOINT . "/$fileIdOrJob";
        } elseif ($fileIdOrJob instanceof Job && $type != null) {
            $href = $fileIdOrJob->getLink(Job::getFileTypeRel($type));
        }

        if ($href == null) {
            return null;
        }

        try {
            return $this->getResource($href, '\Lingo24\API\Model\File');
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Create a file, uploading its name and content. Files created by name are always created as SOURCE files.
     *
     * @param string|File $fileOrName A File object or filename.
     * @param unknown     $content    The file's contents.
     *
     * @return File The created File.
     */
    public function createFile($fileOrName, $filePath)
    {
        $payload = array();
        if ($fileOrName instanceof File) {
            $payload['name'] = $fileOrName->getName();
            $payload['type'] = (string) $fileOrName->getType();
        } else {
            $payload['name'] = $fileOrName;
            $payload['type'] = (string) new FileType("SOURCE");
        }

        $result = $this->postJson($this->getBaseUrl() . self::$FILE_ENDPOINT, $payload);

        $file = new File($result);

        $result = $this->put(
            $file->getLink(File::CONTENT_REL),
            file_get_contents($filePath),
            array('contentType' => 'application/octet-stream')
        );

        return $file;
    }

    /**
     * Delete an existing Lingo24 file.
     *
     * @param File $file The file to delete.
     */
    public function deleteFile(File $file)
    {
        $this->delete($file->getSelf());
    }

    /**
     * Fetch file's contents by id, file or job/type.
     *
     * @param int|File|Job|string $fileIdFileJobOrHref  The id of the file, the file object, the job to fetch a file for
     *                                                  or file content href.
     * @param string              $type                 The file type, source or target.
     *
     * @return Object A file's contents.
     */
    public function getFileContent($fileIdFileJobOrHref, $type = null)
    {
        $href = null;

        if (is_numeric($fileIdFileJobOrHref)) {
            $href =
                $this->getBaseUrl() . self::$FILE_ENDPOINT . "/$fileIdFileJobOrHref/" . Docs::$FILE_CONTENT_ENDPOINT;
        } elseif ($fileIdFileJobOrHref instanceof File) {
            $href = $fileIdFileJobOrHref->getLink(File::CONTENT_REL);
        } elseif ($fileIdFileJobOrHref instanceof Job && $type != null) {
            $fileHref = $fileIdFileJobOrHref->getLink(Job::getFileTypeRel($type));
            if ($fileHref != null) {
                try {
                    $file = $this->getResource($fileHref, '\Lingo24\API\Model\File');
                    $href = $file->getLink(File::CONTENT_REL);
                } catch (Lingo24ApiNotFoundException $e) {
                    return null;
                }
            }
        } elseif (is_string($fileIdFileJobOrHref)) {
            $href = $fileIdFileJobOrHref;
        }

        if ($href == null) {
            return null;
        }

        try {
            return $this->get($href);
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        } catch (Lingo24ApiException $e) {
            return null;
        }
    }

    /**
     * Fetch a list of jobs for a given project.
     *
     * @param int|Project $project A project id or project.
     * @param int         $size    The number of jobs to return, defaults to 10.
     * @param int         $page    The page number, defaults to 0.
     *
     * @return array An array of jobs.
     */
    public function getJobs($project, $size = 10, $page = 0)
    {
        if (is_numeric($project)) {
            $project = $this->getProject($project);
        }

        $results = $this->getJson(
            $project->getLink(Project::JOBS_REL),
            array('size' => $size, 'page' => $page)
        );

        $jobs = array();
        foreach ($results->content as $result) {
            $jobs[] = new Job($result, $project);
        }

        return $jobs;
    }

    /**
     * Fetch a job using its id and load in all linked data.
     *
     * @param int|Project $project A project id or project.
     * @param int         $jobId   The job's id.
     *
     * @return Job The job object with attached linked data.
     */
    public function getJob($project, $jobId)
    {
        if (is_numeric($project)) {
            $project = $this->getProject($project);
        }

        try {
            $result = $this->getJson($project->getLink(Project::JOBS_REL) . "/$jobId");

            $job = new Job($result);
            $job->getProject($this);
            $job->getService($this);
            $job->getSourceLocale($this);
            $job->getTargetLocale($this);
            $job->getSourceFile($this);
            $job->getTargetFile($this);

            return $job;
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Create a new Lingo24 job.
     *
     * @param Job $job A job object to send to the API.
     *
     * @return Job The created job.
     */
    public function createJob($job)
    {
        if ($job->getProject($this) == null
                || $job->getProject($this)->getId() == null) {
            throw new Lingo24ApiException(
                'Cannot create a job with no project, attach a project to the job first and ensure the project has '
                . 'been created.'
            );
        }

        if ($job->getService($this) == null) {
            throw new Lingo24ApiException(
                'Cannot create a job with no service, attach a service to the job first.'
            );
        }

        if ($job->getSourceLocale($this) == null) {
            throw new Lingo24ApiException(
                'Cannot create a job with no source locale, attach a source locale to the job first.'
            );
        }

        if ($job->getTargetLocale($this) == null) {
            throw new Lingo24ApiException(
                'Cannot create a job with no target locale, attach a target locale to the job first.'
            );
        }

        if ($job->getSourceFile($this) == null
                || $job->getSourceFile($this)->getId() == null) {
            throw new Lingo24ApiException(
                'Cannot create a job with no source file, attach a source file to the job first and ensure the file '
                . 'has been uploaded.'
            );
        }


        $payload = array('projectId'      => $job->getProject($this)->getId(),
                         'serviceId'      => $job->getService($this)->getId(),
                         'sourceLocaleId' => $job->getSourceLocale($this)->getId(),
                         'targetLocaleId' => $job->getTargetLocale($this)->getId(),
                         'sourceFileId'   => $job->getSourceFile($this)->getId());

        $result = $this->postJson($job->getProject($this)->getLink(Project::JOBS_REL), $payload);

        return new Job($result, $job->getProject($this));
    }

    /**
     * Update an existing Lingo24 job.
     *
     * @param Job $job An existing job.
     *
     * @return Job The updated job.
     */
    public function updateJob($job)
    {
        if ($job->getId() == null) {
            throw new Lingo24ApiException('Cannot update a job with no id, create the job first.');
        }

        if ($job->getService() == null) {
            throw new Lingo24ApiException('Cannot update a job with no service, attach a service to the job first.');
        }

        if ($job->getSourceLocale() == null) {
            throw new Lingo24ApiException(
                'Cannot update a job with no source locale, attach a source locale to the job first.'
            );
        }

        if ($job->getTargetLocale() == null) {
            throw new Lingo24ApiException(
                'Cannot update a job with no target locale, attach a target locale to the job first.'
            );
        }

        if ($job->getSourceFile() == null
                || $job->getSourceFile()->getId() == null) {
            throw new Lingo24ApiException(
                'Cannot update a job with no source file, attach a source file to the job first and ensure the file '
                . 'has been uploaded.'
            );
        }

        $payload = array('id'             => $job->getId(),
                         'projectId'      => $job->getProject()->getId(),
                         'serviceId'      => $job->getService()->getId(),
                         'sourceLocaleId' => $job->getSourceLocale()->getId(),
                         'targetLocaleId' => $job->getTargetLocale()->getId(),
                         'sourceFileId'   => $job->getSourceFile()->getId(),
                         'jobStatus'      => $job->getStatus()->getValue());

        $result = $this->putJson($job->getSelf(), $payload);

        return new Job($result);
    }

    /**
     * Delete an existing Lingo24 job.
     *
     * @param Job $job The job to delete.
     */
    public function deleteJob(Job $job)
    {
        if ($job->getStatus()->equals(JobStatus::IN_PROGRESS)) {
            throw new Lingo24ApiException('The job is confirmed, you cannot delete it.');
        }

        $this->delete($job->getSelf());
    }

    /**
     * Fetch metrics for a job.
     *
     * @param Job|string $jobOrHref Job object for a href for the metrics.
     *
     * @return array A map of metrics.
     */
    public function getJobMetrics($jobOrHref)
    {
        if ($jobOrHref instanceof Job) {
            $jobOrHref = $jobOrHref->getLink(Job::METRICS_REL);
        }

        try {
            $results = $this->getJson($jobOrHref);

            $metrics = array();
            if ($results != null && !is_null($results->values)) {
                foreach ($results->values as $key => $result) {
                    $metrics[$key] = new JobMetric($result);
                }
            }

            return $metrics;
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Fetch a price for a job.
     *
     * @param Job|string $jobOrHref Job object for a href for the price.
     *
     * @return JobPrice The job price.
     */
    public function getJobPrice($jobOrHref)
    {
        if ($jobOrHref instanceof Job) {
            $jobOrHref = $jobOrHref->getLink(Job::PRICE_REL);
        }

        try {
            $result = $this->getJson($jobOrHref);
            $jobPrice = null;
            if ($result != null) {
                $jobPrice = new JobPrice($result);
            }

            return $jobPrice;
        } catch (Lingo24ApiNotFoundException $e) {
            return null;
        }
    }

    /**
     * Fetch the list of available tags from the API.
     *
     * @return array An array of tags.
     */
    public function getTags()
    {
        if ($this->tags == null) {
            $results = $this->getJson($this->getBaseUrl() . self::$TAGS_ENDPOINT);

            if ($results->page->totalElements > count($results->content)) {
                $results = $this->getJson(
                    $this->getBaseUrl() . self::$TAGS_ENDPOINT,
                    array('size' => $results->page->totalElements)
                );
            }

            $this->tags = $results->content;
        }

        return $this->tags;
    }

    /**
     * Return the API's base url.
     *
     * @return string The base url.
     */
    public function getBaseUrl()
    {
        if ($this->getCustomBaseUrl() != null) {
            return $this->getCustomBaseUrl();
        }

        if ($this->getEnv() == self::$DEMO_ENV) {
            return self::$BASE_URL_DEMO;
        }

        return self::$BASE_URL_LIVE;
    }

    /**
     * Return the Ease OAuth2 authorise url.
     *
     * @return string The Ease Oauth2 authorise url.
     */
    public function getAuthUrl()
    {
        if ($this->getEnv() == self::$DEMO_ENV) {
            return self::$AUTH_URL_DEMO;
        }

        return self::$AUTH_URL_LIVE;
    }
}
