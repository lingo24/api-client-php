<?php

namespace Lingo24\API\Model;

/**
 * A project's status.
 */
class JobStatus
{
    /**
     * The job has been created in our system and is waiting to be analyzed.
     */
    const NEW_STATUS = 1;

    /**
     * The job is being analyzed. Switching to this status from NEW is done automatically.
     */
    const ANALYZING = 2;

    /**
     * The job has been analyzed and has both metrics and price available.
     */
    const QUOTED = 3;

    /**
     * The job is currently underway at Lingo24.
     */
    const IN_PROGRESS = 4;

    /**
     * The job is now finished, with the target files available for collection via the File resource.
     */
    const TRANSLATED = 5;

    /**
     * The job has been cancelled.
     */
    const CANCELLED = 6;

    /**
     * The job has was unable to be analysed.
     */
    const ANALYZING_PROBLEMS = 7;

    /**
     * @var Internal representation of a for the status value.
     */
    private $value;

    /**
     * Create a status object based on a string.
     *
     * @param string $status The status as a string.
     */
    public function __construct($status)
    {
        if ($status == "ANALYZING") {
            $this->value = self::ANALYZING;
        } elseif ($status == "QUOTED") {
            $this->value = self::QUOTED;
        } elseif ($status == "IN_PROGRESS") {
            $this->value = self::IN_PROGRESS;
        } elseif ($status == "TRANSLATED") {
            $this->value = self::TRANSLATED;
        } elseif ($status == "CANCELLED") {
            $this->value = self::CANCELLED;
        } elseif ($status == "ANALYZING_PROBLEMS") {
            $this->value = self::ANALYZING_PROBLEMS;
        } else {
            $this->value = self::NEW_STATUS;
        }
    }

    /**
     * Return the job status value as a string. Returns cancelled for any unknown status.
     *
     * @return string|NULL The project status value.
     */
    public function getValue()
    {
        if ($this->value == self::ANALYZING) {
            return "ANALYZING";
        } elseif ($this->value == self::QUOTED) {
            return "QUOTED";
        } elseif ($this->value == self::IN_PROGRESS) {
            return "IN_PROGRESS";
        } elseif ($this->value == self::TRANSLATED) {
            return "TRANSLATED";
        } elseif ($this->value == self::CANCELLED) {
            return "CANCELLED";
        } elseif ($this->value == self::ANALYZING_PROBLEMS) {
            return "ANALYZING_PROBLEMS";
        } else {
            return "NEW";
        }
    }

    /**
     * Return the job status as a string. Returns cancelled for any unknown status.
     *
     * @return string|NULL The project status.
     */
    public function __toString()
    {
        if ($this->value == self::ANALYZING) {
            return "Analyzing";
        } elseif ($this->value == self::QUOTED) {
            return "Quoted";
        } elseif ($this->value == self::IN_PROGRESS) {
            return "In progress";
        } elseif ($this->value == self::TRANSLATED) {
            return "Translated";
        } elseif ($this->value == self::CANCELLED) {
            return "Cancelled";
        } elseif ($this->value == self::ANALYZING_PROBLEMS) {
            return "Analyzing problems";
        } else {
            return "New";
        }
    }

    /**
     * Check a job status is equal to another, or to a job status value.
     *
     * @param int|JobStatus $value The value to compare against.
     *
     * @return boolean
     */
    public function equals($value)
    {
        if ($value instanceof JobStatus) {
            return $this->value == $value->value;
        } else {
            return $this->value == $value;
        }
    }
}
