<?php

namespace Lingo24\API\Model;

/**
 * Price for a project analysis.
 */
class ProjectPrice extends Resource
{
    /**
     * @var The currency code for price.
     */
    private $currency;

    /**
     * @var The total price without VAT with discount.
     */
    private $totalWoVatWDiscount;

    /**
     * @var The total price without VAT without discount.
     */
    private $totalWoVatWoDiscount;

    /**
     * @var The total price with VAT with discount.
     */
    private $totalWVatWDiscount;

    /**
     * @var The total price with VAT without discount.
     */
    private $totalWVatWoDiscount;

    /**
     * Create a ProjectPrice object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the project price.
     */

    public function __construct($object)
    {
        $this->currency             = $object->currencyCode;
        $this->totalWoVatWDiscount  = $object->totalWoVatWDiscount;
        $this->totalWoVatWoDiscount = $object->totalWoVatWoDiscount;
        $this->totalWVatWDiscount   = $object->totalWVatWDiscount;
        $this->totalWVatWoDiscount  = $object->totalWVatWoDiscount;
    }

    /**
     * Setter method for the currency.
     *
     * @param string $currency The price currency string.
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * Getter method for the currency.
     *
     * @return string The price currency.
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Setter method for total price with VAT and discount.
     *
     * @param float $totalWVatWDiscount Total price with VAT and discount.
     */
    public function setTotalWVatWDiscount($totalWVatWDiscount)
    {
        $this->totalWVatWDiscount = $totalWVatWDiscount;
    }

    /**
     * Getter method for total price with VAT and discount.
     *
     * @return float The total price with vat with discount.
     */
    public function getTotalWVatWDiscount()
    {
        return $this->totalWVatWDiscount;
    }

    /**
     * Setter method for total price with VAT and without discount.
     *
     * @param float $totalWVatWoDiscount Total price with VAT and without discount.
     */
    public function setTotalWVatWoDiscount($totalWVatWoDiscount)
    {
        $this->totalWVatWoDiscount = $totalWVatWoDiscount;
    }

    /**
     * Getter method for total price with VAT and without discount.
     *
     * @return float The total price with vat without discount.
     */
    public function getTotalWVatWoDiscount()
    {
        return $this->totalWVatWoDiscount;
    }

    /**
     * Setter method for total price without VAT and with discount.
     *
     * @param float $totalWoVatWDiscount Total price without VAT and with discount.
     */
    public function setTotalWoVatWDiscount($totalWoVatWDiscount)
    {
        $this->totalWoVatWDiscount = $totalWoVatWDiscount;
    }

    /**
     * Getter method for total price without VAT and with discount.
     *
     * @return float The total price without vat with discount.
     */
    public function getTotalWoVatWDiscount()
    {
        return $this->totalWoVatWDiscount;
    }

    /**
     * Setter method for total price without VAT and without discount.
     *
     * @param float $totalWoVatWoDiscount Total price without VAT and discount.
     */
    public function setTotalWoVatWoDiscount($totalWoVatWoDiscount)
    {
        $this->totalWoVatWoDiscount = $totalWoVatWoDiscount;
    }

    /**
     * Getter method for total price without VAT and discount.
     *
     * @return float The total price without vat without discount.
     */
    public function getTotalWoVatWoDiscount()
    {
        return $this->totalWoVatWoDiscount;
    }
}
