<?php

namespace Lingo24\API\Model;

/**
 * A project's status.
 */
class ProjectStatus
{
    /**
     * @const The project has been created in our system, ready for configuration. In this status you can manipulate
     *        jobs.
     */
    const CREATED = 1;

    /**
     * @const The project has been submitted for a quote, which will generated automatically when ready.
     */
    const PENDING = 6;

    /**
     * @const This is an optional status. Updating a project to this status saves a quote in our system. In this status
     *        you cannot further manipulate the project. A typical quote is valid for 15 days, although this will depend
     *        on individual terms and conditions.
     */
    const QUOTED = 2;

    /**
     * @const A project will be in this status when it has been accepted by the client. Changing it to this status
     *        triggers Lingo24 to start work on your project.
     */
    const IN_PROGRESS = 3;

    /**
     * @const This status is when a project has been cancelled by the client. Changing the project to this status tells
     *        Lingo24 to cancel the project. Project cancellations are covered by our standard terms and conditions.
     */
    const CANCELLED = 4;

    /**
     * @const Projects move to this status once all of the project's jobs have been completed.
     */
    const FINISHED = 5;

    /**
     * @const Internal representation of a for the status value.
     */
    private $value;

    /**
     * Create a status object based on a string.
     *
     * @param string $status The status as a string.
     */
    public function __construct($status)
    {
        if (is_string($status)) {
            if ($status == 'PENDING') {
                $this->value = self::PENDING;
            } elseif ($status == 'QUOTED') {
                $this->value = self::QUOTED;
            } elseif ($status == 'IN_PROGRESS') {
                $this->value = self::IN_PROGRESS;
            } elseif ($status == 'CANCELLED') {
                $this->value = self::CANCELLED;
            } elseif ($status == 'FINISHED') {
                $this->value = self::FINISHED;
            } else {
                $this->value = self::CREATED;
            }
        } elseif (is_int($status)) {
            $this->value = $status;
        }
    }

    /**
     * Return the project status value as a string.
     *
     * @return string|NULL The project status value.
     */
    public function getValue()
    {
        $return = '';

        if ($this->value == self::CREATED) {
            $return = 'CREATED';
        } elseif ($this->value == self::PENDING) {
            $return = 'PENDING';
        } elseif ($this->value == self::QUOTED) {
            $return = 'QUOTED';
        } elseif ($this->value == self::IN_PROGRESS) {
            $return = 'IN_PROGRESS';
        } elseif ($this->value == self::CANCELLED) {
            $return = 'CANCELLED';
        } elseif ($this->value == self::FINISHED) {
            $return = 'FINISHED';
        }

        return $return;
    }

    /**
     * Return the project status as a string.
     *
     * @return string|NULL The project status.
     */
    public function __toString()
    {
        return ucfirst(strtolower(str_replace('_', ' ', $this->getValue())));
    }

    /**
     * Check a project status is equal to another, or to a project status value.
     *
     * @param int|ProjectStatus $value The value to compare against.
     *
     * @return boolean True if the statuses are equal.
     */
    public function equals($value)
    {
        if ($value instanceof ProjectStatus) {
            return $this->value == $value->value;
        } else {
            return $this->value == $value;
        }
    }

    /**
     * Compare this project status against another based on it's value.
     *
     * @param int|ProjectStatus $value The value to compare against.
     *
     * @return int Positive if greater than, negative if less than, zero if equal.
     */
    public function cmp($value)
    {
        if ($value instanceof ProjectStatus) {
            return $this->value - $value->value;
        } else {
            return $this->value - $value;
        }
    }
}
