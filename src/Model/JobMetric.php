<?php

namespace Lingo24\API\Model;

/**
 * Metrics for a job analysis.
 */
class JobMetric
{
    /**
     * @var The character count for this metric.
     */
    private $characters;

    /**
     * @var The segment count for this metric.
     */
    private $segments;

    /**
     * @var The word count for this metric.
     */
    private $words;

    /**
     * @var The white space count for this metric.
     */
    private $whiteSpaces;

    /**
     * Create a JobMetric object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the job metric.
     */
    public function __construct($object)
    {
        $this->characters  = $object->CHARACTERS;
        $this->segments    = $object->SEGMENTS;
        $this->words       = $object->WORDS;
        $this->whiteSpaces = $object->WHITE_SPACES;
    }

    /**
     * Getter method for the character count.
     *
     * @return The character count.
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Setter method for the character count.
     *
     * @param int $characters The character count.
     */
    public function setCharacters($characters)
    {
        $this->characters = $characters;
    }

    /**
     * Getter method for the segment count.
     *
     * @return The segment count.
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * Setter method for the segment count.
     *
     * @param int $segments The segment count.
     */
    public function setSegments($segments)
    {
        $this->segments = $segments;
    }

    /**
     * Getter method for the word count.
     *
     * @return The word count.
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * Setter method for the word count.
     *
     * @param int $words The word count.
     */
    public function setWords($words)
    {
        $this->words = $words;
    }

    /**
     * Getter method for the white space count.
     *
     * @return The white space count.
     */
    public function getWhiteSpaces()
    {
        return $this->whiteSpaces;
    }

    /**
     * Setter method for the white space count.
     *
     * @param int $whiteSpaces The white space count.
     */
    public function setWhiteSpaces($whiteSpaces)
    {
        $this->whiteSpaces = $whiteSpaces;
    }
}
