<?php

namespace Lingo24\API\Model;

/**
 * A project charge or discount.
 */
class ProjectCharge extends Resource
{
    /**
     * @var string The charge title.
     */
    private $title;

    /**
     * @var double The charge value.
     */
    private $value;

    /**
     * Create a ProjectCharge object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the project charge.
     */
    public function __construct($object)
    {
        $this->title = $object->title;
        $this->value = $object->value;
    }

    /**
     * Getter method for the title.
     *
     * @return string The title.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setter method for the title.
     *
     * @param string $title The new title.
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Getter method for the value.
     *
     * @return string The value.
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Setter method for the value.
     *
     * @param double $value The new value.
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
