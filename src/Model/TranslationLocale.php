<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * Lingo24 locale - supported locales.
 */
class TranslationLocale extends Resource
{
    /**
     * @var The locale id.
     */
    private $id;

    /**
     * @var The locale language.
     */
    private $language;

    /**
     * @var The locale country.
     */
    private $country;

    /**
     * Create a TranslationLocale object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the Locale.
     */
    public function __construct($object)
    {
        $this->id       = $object->id;
        $this->language = $object->language;
        $this->country  = $object->country;

        parent::__construct($object);
    }

    /**
     * Getter method for the id.
     *
     * @return int The locale's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The locale's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the language.
     *
     * @return string The locale's language.
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Setter method for the language.
     *
     * @param string $language The locale's language.
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * Getter method for the country.
     *
     * @return string The locale's country.
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Setter method for the country.
     *
     * @param string $country The locale's country.
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Return a string representation of this locale.
     *
     * @return string The language and country.
     */
    public function __toString()
    {
        return $this->language . '_' . $this->country;
    }

    /**
     * TranslationLocale comparison function, string comparison on the result of the toString().
     *
     * @param TranslationLocale $a The first locale to be compared.
     * @param TranslationLocale $b The second locale to be compared.
     *
     * @return int Less than 0 if $a is less than $b; greater than 0 if $a is greater than $b, and 0 if they are equal.
     */
    public static function cmp($a, $b)
    {
        return strcmp($a->__toString(), $b->__toString());
    }
}
