<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * Files uploaded to, or retrieved by Ease.
 */
class File extends Resource
{
    /**
     * File content link relationship.
     */
    const CONTENT_REL = 'content';

    /**
     * @var The file id.
     */
    private $id;

    /**
     * @var The filename.
     */
    private $name;

    /**
     * @var The file's type.
     */
    private $type;

    /**
     * Create a File object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object  Source object for the file.
     * @param string   $baseUrl Optional base URL for hyperlinks.
     */
    public function __construct($object, $baseUrl = '')
    {
        $this->id   = $object->id;
        $this->name = $object->name;
        $this->type = new FileType($object->type);

        parent::__construct($object);
    }

    /**
     * Getter method for the id.
     *
     * @return int The file's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The file's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the filename.
     *
     * @return string The filename.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter method for the filename.
     *
     * @param string $name The file's name.
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Getter method for the type.
     *
     * @return string The file's type.
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Setter method for the type.
     *
     * @param string|FileType $type The fikle's type.
     */
    public function setType($type)
    {
        if ($type instanceof FileType) {
            $this->type = $type;
        } else {
            $this->type = new FileType($type);
        }
    }

    /**
     * Returns a string representation of a file.
     *
     * @return string The filename.
     */
    public function __toString()
    {
        return $this->name != null ? $this->name : '';
    }
}
