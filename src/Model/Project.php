<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * A Lingo24 project, the main unit of work submitted for translation.
 */
class Project extends Resource
{
    /**
     * Project relationships.
     */
    const JOBS_REL    = 'jobs';
    const PRICE_REL   = 'price';
    const CHARGES_REL = 'charges';

    /**
     * @var The project id.
     */
    public $id;

    /**
     * @var The project name.
     */
    private $name;

    /**
     * @var The project created date.
     */
    private $created;

    /**
     * @var The project status.
     */
    private $status;

    /**
     * @var array The project's tags.
     */
    private $tags;

    /**
     * Create a Project object from a stdClass object (e.g. created from a JSON string).
     *
     * @param \stdClass $object Source object for the project.
     */
    public function __construct($object)
    {
        if (isset($object->id)) {
            $this->id = $object->id;
        }
        $this->name = $object->name;
        if (isset($object->created)) {
            $this->created = (new \DateTime())->setTimestamp($object->created);
        }
        if (isset($object->projectStatus)) {
            $this->status = new ProjectStatus($object->projectStatus);
        }
        if (isset($object->tags)) {
            $this->setTags($object->tags);
        }

        parent::__construct($object);
    }

    /**
     * Getter method for the id.
     *
     * @return int The project's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The project's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the name.
     *
     * @return string The project's name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter method for the name.
     *
     * @param string $name The project's name.
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Getter method for the created date.
     *
     * @return int The project's created date.
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Setter method for the created date.
     *
     * @param int|DateTime $created The project's created date.
     */
    public function setCreated($created)
    {
        if ($created instanceof \DateTime) {
            $this->created = $created;
        } else {
            $this->created = (new \DateTime())->setTimestamp($created);
        }
    }

    /**
     * Getter method for the status.
     *
     * @return string The project's status.
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Setter method for the status.
     *
     * @param string|ProjectStatus $status The project's status.
     */
    public function setStatus($status)
    {
        if ($status instanceof ProjectStatus) {
            $this->status = $status;
        } else {
            $this->status = new ProjectStatus($status);
        }
    }

    /**
     * Getter methods for the project's tags.
     *
     * @return array An array of tags.
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Setter method for the project's tags.
     *
     * @param $tags array A new list of tags.
     */
    public function setTags($tags)
    {
        if (is_array($tags)) {
            $this->tags = $tags;
        }
    }

    /**
     * Adds a tag to the project.
     *
     * @param $tag String The tag to add.
     */
    public function addTag($tag)
    {
        if (!in_array($tag, $this->tags)) {
            $this->tags[] = $tag;
        }
    }

    /**
     * Return the project name as a string representation.
     *
     * @return string The project's name.
     */
    public function __toString()
    {
        return $this->name;
    }
}
