<?php

namespace Lingo24\API\Model;

/**
 * Model for OAuth2 authorisation details.
 */
class OAuth2
{
    /**
     * @var String The OAuth2 access token.
     */
    private $accessToken;

    /**
     * @var String The OAuth2 refresh token.
     */
    private $refreshToken;

    /**
     * @var Expiry date for the access token.
     */
    private $expiryDate;

    /**
     * Create an OAuth2 model object from a API access response.
     *
     * @param Object $oauth2Response API access response object.
     */
    public function __construct($oauth2Response)
    {
        $this->accessToken  = $oauth2Response->access_token;
        $this->refreshToken = $oauth2Response->refresh_token;
        $this->expiryDate   = time() + $oauth2Response->expires_in;
    }

    /**
     * Check if the access token is still valid, based on its expiry date.
     *
     * @return boolean True if valid, false otherwise.
     */
    public function isValid()
    {
        return $this->expiryDate > time();
    }

    /**
     * Getter function for the access token.
     *
     * @return string The OAuth2 access token.
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Getter function for the refresh token.
     *
     * @return string The OAuth2 refresh token.
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * Getter function for the access token expiry date.
     *
     * @return Date The access token expiry date.
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }
}
