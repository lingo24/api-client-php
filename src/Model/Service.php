<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * Lingo24 service - the type of service requested for a job.
 */
class Service extends Resource
{
    /**
     * @var The service id.
     */
    private $id;

    /**
     * @var The service name.
     */
    private $name;

    /**
     * @var The service description.
     */
    private $description;

    /**
     * Create a Service object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the Service.
     */
    public function __construct($object)
    {
        $this->id          = $object->id;
        $this->name        = $object->name;
        $this->description = $object->description;

        parent::__construct($object);
    }

    /**
     * Getter method for the id.
     *
     * @return int The service's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The service's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the name.
     *
     * @return string The service's name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter method for the name.
     *
     * @param string $name The service's name.
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Getter method for the description.
     *
     * @return string The service's description.
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter method for the description.
     *
     * @param string $description The service's description.
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Return a representation of the service as a string.
     *
     * @return string The service name.
     */
    public function __toString()
    {
        return $this->name;
    }
}
