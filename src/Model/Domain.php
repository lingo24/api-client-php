<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * Lingo24 domain - supported Lingo24 business domains.
 */
class Domain extends Resource
{
    /**
     * @var The domain id.
     */
    private $id;

    /**
     * @var The domain name.
     */
    private $name;

    /**
     * Create a Domain object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass $object Source object for the domain.
     */
    public function __construct($object)
    {
        $this->id   = $object->id;
        $this->name = $object->name;

        parent::__construct($object);
    }

    /**
     * Getter method for the id.
     *
     * @return int The domain's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The domain's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the name.
     *
     * @return string The domain's name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter method for the name.
     *
     * @param string $name The domain's name.
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Return the name as string representation of this domain.
     *
     * @return string The domain's name.
     */
    public function __toString()
    {
        return $this->name;
    }
}
