<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

/**
 * A job is a unit of work to be translated: a source file, from a source locale to a target locale.
 */
class Job extends Resource
{
    /**
     * Relationships.
     */
    const METRICS_REL       = 'metrics';
    const PROJECT_REL       = 'project';
    const SERVICE_REL       = 'service';
    const SOURCE_LOCALE_REL = 'source-locale';
    const TARGET_LOCALE_REL = 'target-locale';
    const SOURCE_FILE_REL   = 'source-file';
    const TARGET_FILE_REL   = 'target-file';
    const PRICE_REL         = 'price';

    /**
     * @var The job id.
     */
    private $id;

    /**
     * @var The job's status.
     */
    private $status;

    /**
     * @var The job's project.
     */
    private $project;

    /**
     * @var The service selected for the job.
     */
    private $service;

    /**
     * @var The source locale for the job.
     */
    private $sourceLocale;

    /**
     * @var The target locale for the job.
     */
    private $targetLocale;

    /**
     * @var The source file for the job.
     */
    private $sourceFile;

    /**
     * @var The target file for the job.
     */
    private $targetFile;

    /**
     * Create a Job object from a stdClass object (e.g. created from a JSON string).
     *
     * @param stdClass|Project $objectOrProject Source object for the job, or the job's project.
     * @param Project          $project         The job's project.
     */
    public function __construct($objectOrProject = null, Project $project = null)
    {
        if ($objectOrProject instanceof Project) {
            $this->project = $objectOrProject;

            $this->addLink(self::PROJECT_REL, $this->project->getLink(Project::SELF_REL));
        } else {
            $this->id      = $objectOrProject->id;
            $this->status  = new JobStatus($objectOrProject->jobStatus);
            $this->project = $project;

            parent::__construct($objectOrProject);
        }
    }

    /**
     * Getter method for the id.
     *
     * @return int The job's id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Setter method for the id.
     *
     * @param int $id The job's id.
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Getter method for the status.
     *
     * @return string The job's status.
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Setter method for the status.
     *
     * @param string|ProjectStatus $status The job's status.
     */
    public function setStatus($status)
    {
        if ($status instanceof JobStatus) {
            $this->status = $status;
        } else {
            $this->status = new JobStatus($status);
        }
    }

    /**
     * Getter method for the project.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return Project The job's project.
     */
    public function getProject($docs = null)
    {
        if ($this->project != null) {
            return $this->project;
        }

        $href = $this->getLink(self::PROJECT_REL);

        if ($docs != null
                && $href != null) {
            $this->project = $docs->getResource($href, '\Lingo24\API\Model\Project');
            return $this->project;
        }

        return null;
    }

    /**
     * Setter method for the project.
     *
     * @param Project $project The job's project.
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Getter method for the service.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return Service The job's service.
     */
    public function getService($docs = null)
    {
        if ($this->service != null) {
            return $this->service;
        }

        $href = $this->getLink(self::SERVICE_REL);
        if ($docs != null
                && $href != null) {
            $this->service = $docs->getResource($href, '\Lingo24\API\Model\Service');
            return $this->service;
        }

        return null;
    }

    /**
     * Setter method for the service.
     *
     * @param Service $service The job's service.
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * Getter method for the source locale.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return TranslationLocale The job's source locale.
     */
    public function getSourceLocale($docs = null)
    {
        if ($this->sourceLocale != null) {
            return $this->sourceLocale;
        }

        $href = $this->getLink(self::SOURCE_LOCALE_REL);
        if ($docs != null
                && $href != null) {
            $this->sourceLocale = $docs->getResource($href, '\Lingo24\API\Model\TranslationLocale');
            return $this->sourceLocale;
        }

        return null;
    }

    /**
     * Setter method for the source locale.
     *
     * @param TranslationLocale $sourceLocale The job's source locale.
     */
    public function setSourceLocale($sourceLocale)
    {
        $this->sourceLocale = $sourceLocale;
    }

    /**
     * Getter method for the target locale.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return TranslationLocale The job's target locale.
     */
    public function getTargetLocale($docs = null)
    {
        if ($this->targetLocale != null) {
            return $this->targetLocale;
        }

        $href = $this->getLink(self::TARGET_LOCALE_REL);
        if ($docs != null
                && $href != null) {
            $this->targetLocale = $docs->getResource($href, '\Lingo24\API\Model\TranslationLocale');
            return $this->targetLocale;
        }

        return null;
    }

    /**
     * Setter method for the source file.
     *
     * @param File $sourceFile The job's source file.
     */
    public function setSourceFile($sourceFile)
    {
        $this->sourceFile = $sourceFile;
    }

    /**
     * Getter method for the source file.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return File The job's source file.
     */
    public function getSourceFile($docs = null)
    {
        if ($this->sourceFile != null) {
            return $this->sourceFile;
        }

        $href = $this->getLink(self::SOURCE_FILE_REL);
        if ($docs != null
                && $href != null) {
            $this->sourceFile = $docs->getResource($href, '\Lingo24\API\Model\File');
            return $this->sourceFile;
        }

        return null;
    }

    /**
     * Setter method for the target locale.
     *
     * @param TranslationLocale $targetLocale The job's target locale.
     */
    public function setTargetLocale($targetLocale)
    {
        $this->targetLocale = $targetLocale;
    }

    /**
     * Getter method for the target file.
     *
     * @param Docs $docs A Business Documents API client.
     *
     * @return File The job's target file.
     */
    public function getTargetFile($docs = null)
    {
        if ($this->targetFile != null) {
            return $this->targetFile;
        }

        $href = $this->getLink(self::TARGET_FILE_REL);
        if ($docs != null
                && $href != null) {
            $this->targetFile = $docs->getResource($href, '\Lingo24\API\Model\File');
            return $this->targetFile;
        }

        return null;
    }

    /**
     * Setter method for the target file.
     *
     * @param File $targetFile The job's target file.
     */
    public function setTargetFile($targetFile)
    {
        $this->targetFile = $targetFile;
    }

    /**
     * Get the relationship key for a particular type of file.
     *
     * @param string $type
     *
     * @return string|NULL
     */
    public static function getFileTypeRel($type)
    {
        if ($type == FileType::SOURCE) {
            return self::SOURCE_FILE_REL;
        } elseif ($type == FileType::TARGET) {
            return self::TARGET_FILE_REL;
        } else {
            return null;
        }
    }
}
