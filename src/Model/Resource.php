<?php

namespace Lingo24\API\Model;

/**
 * Helper class for resource objects containing HATEOAS hyperlink maps.
 */
abstract class Resource
{
    /**
     * The self link relationship.
     */
    const SELF_REL = 'self';

    /**
     * @var The hyperlinks map.
     */
    private $links = array();

    /**
     * Loads links for a source object if available.
     *
     * @param \stdClass $object Source object.
     */
    public function __construct($object)
    {
        if (isset($object->links)) {
            foreach ($object->links as $link) {
                if ($link->rel != null && $link->href != null) {
                    $this->links[$link->rel] = $link->href;
                }
            }
        }
    }

    /**
     * Add a hyperlink to the map.
     *
     * @param string $ref The hyperlink relationship.
     * @param string $href the hyperlink URI.
     */
    public function addLink($rel, $href)
    {
        $this->links[$rel] = $href;
    }

    /**
     * Add an array of hyperlinks to the map.
     *
     * @param array $links Array of links.
     */
    public function addLinks($links)
    {
        foreach ($links as $link) {
            $this->links[$link->rel] = $link->href;
        }
    }

    /**
     * Return the hyperlinks map.
     *
     * @return array A map of hyperlinks.
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Return a specific hyperlink based on its relationship.
     *
     * @param string $rel The hyperlink relationship.
     *
     * @return string|null The hyperlink URI.
     */
    public function getLink($rel)
    {
        if (isset($this->links[$rel])) {
            return $this->links[$rel];
        } else {
            return null;
        }
    }

    /**
     * Return the self link for this resource.
     *
     * @return string|null The self URI.
     */
    public function getSelf()
    {
        return $this->getLink(self::SELF_REL);
    }
}
