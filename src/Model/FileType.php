<?php

namespace Lingo24\API\Model;

/**
 * A file's type.
 */
class FileType
{
    /**
     * Source file, with content to be translated.
     */
    const SOURCE = 1;

    /**
     * Target file, containing translated content.
     */
    const TARGET = 2;

    /**
     * @var Internal representation of a for the file type.
     */
    private $value;

    /**
     * Create a file type object based on a string.
     *
     * @param string $type The type as a string.
     */
    public function __construct($type)
    {
        if ($type == 'TARGET') {
            $this->value = self::TARGET;
        } else {
            $this->value = self::SOURCE;
        }
    }

    /**
     * Return the file type as a string.
     *
     * @return string|NULL The file type.
     */
    public function __toString()
    {
        if ($this->value == self::TARGET) {
            return 'TARGET';
        } else {
            return 'SOURCE';
        }
    }

    /**
     * Check a file type is equal to another, or to a file type value.
     *
     * @param int|FileType $value The value to compare against.
     *
     * @return boolean
     */
    public function equals($value)
    {
        if ($value instanceof FileType) {
            return $this->value == $value->value;
        } else {
            return $this->value == $value;
        }
    }
}
