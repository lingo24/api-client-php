<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiAuthorisationException;
use Lingo24\API\Exception\Lingo24ApiException;
use Lingo24\API\Exception\Lingo24ApiNotAllowedException;
use Lingo24\API\Exception\Lingo24ApiNotFoundException;
use Lingo24\API\Model\OAuth2;
use Lingo24\API\Model\Resource;

/**
 * Generic API client functions for API services using OAuth2.
 */
abstract class AbstractOAuth2Client extends AbstractClient
{
    private static $HTTP_HEADER_AUTHORIZATION_BEARER = 'Authorization: Bearer ';
    private static $CURLINFO_HTTP_CODE_STRING        = 'http_code';
    private static $OPTIONS_CONTENT_TYPE             = 'contentType';

    /**
     * @var string $DEMO_ENV Identifier for the demo environment.
     */
    public static $DEMO_ENV = 'demo';

    /**
     * @var string $DEMO_ENV Identifier for the live environment.
     */
    public static $LIVE_ENV = 'live';

    /**
     * @var string $ACCESS_ENDPOINT End point for OAuth2 access requests.
     */
    private static $ACCESS_ENDPOINT = 'oauth2/access';

    /**
     * @var string The current environment configured for this client.
     */
    private $env;

    /**
     * @var string Custom base url, if set.
     */
    private $customBaseUrl;

    /**
     * @var string The API client id.
     */
    private $clientId;

    /**
     * @var string The API client secret.
     */
    private $clientSecret;

    /**
     * @var string The API redirect URI.
     */
    private $redirectUri;

    /**
     * @var OAuth2 OAuth2 authorisation details for the API.
     */
    private $oauth2;

    /**
     * Constructor for the API client. Client id, client secret and the redirect URI are required, optionally an
     * authorisation code or OAuth2 model object can be supplied to authorise the client.
     *
     * @param String        $clientId         The API client ID.
     * @param String        $clientSecret     The API client secret.
     * @param String        $redirectUri      The API redirect URI.
     * @param String|OAuth2 $authCodeOrOAuth2 Optional authorisation code or OAuth2 model object to authorise the
     *                                        client.
     * @param String        $env              The API environment to use.
     * @param String        $customBaseUrl    A custom base URL to use.
     */
    public function __construct(
        $clientId,
        $clientSecret,
        $redirectUri,
        $authCodeOrOAuth2 = null,
        $env = null,
        $customBaseUrl = null
    ) {
        $this->env           = $env;
        $this->customBaseUrl = $customBaseUrl;

        if ($clientId != null && $clientSecret != null && $redirectUri != null) {
            $this->clientId     = $clientId;
            $this->clientSecret = $clientSecret;
            $this->redirectUri  = $redirectUri;

            if ($authCodeOrOAuth2 != null) {
                $auth = $this->authorise($authCodeOrOAuth2);
                if (!$auth) {
                    throw new Lingo24ApiAuthorisationException("Authentication failed while creating client.");
                }
            }
        } else {
            throw new Lingo24ApiException('Client id, client secret and redirect URI are required.');
        }
    }

    /**
     * Authorise the client with the API. If an authorisation code is supplied then send a request for an access token
     * and if successful save details as an OAuth2 model. When an OAuth2 model is supplied, check if it is valid, and if
     * not send a request for a refresh. Return true if the authorisation is successful or false otherwise.
     *
     * @param string|Oauth2 $authCodeOrOAuth2 Authorisation code or OAuth2 model object to authorise the client.
     *
     * @return boolean True if the authorisation was successful.
     */
    public function authorise($authCodeOrOAuth2)
    {
        if ($authCodeOrOAuth2 instanceof OAuth2) {
            if ($authCodeOrOAuth2->isValid()) {
                $this->oauth2 = $authCodeOrOAuth2;
            } else {
                $params = array('client_id'     => $this->clientId,
                                'client_secret' => $this->clientSecret,
                                'redirect_uri'  => $this->redirectUri,
                                'grant_type'    => 'refresh_token',
                                'refresh_token' => $authCodeOrOAuth2->getRefreshToken());

                try {
                    $response = $this->post($this->getBaseUrl() . self::$ACCESS_ENDPOINT, $params, true);
                    $this->oauth2 = new OAuth2($response);
                } catch (Lingo24ApiAuthorisationException $e) {
                    error_log($e);
                    return false;
                }
            }
        } else {
            $params = array('client_id'     => $this->clientId,
                            'client_secret' => $this->clientSecret,
                            'redirect_uri'  => $this->redirectUri,
                            'grant_type'    => 'authorization_code',
                            'code'          => $authCodeOrOAuth2);

            try {
                $response = $this->post($this->getBaseUrl() . self::$ACCESS_ENDPOINT, $params, true);
                $this->oauth2 = new OAuth2($response);
            } catch (Lingo24ApiAuthorisationException $e) {
                error_log($e);
                return false;
            }
        }

        return true;
    }

    /**
     * Performs a GET request to the API using cURL.
     *
     * @param string  $url    The API URL to make a GET request to.
     * @param array   $params An array of parameters to be passed with the GET request.
     * @param boolean $noAuth Skip authentication for this request.
     *
     * @return Object The raw content returned by the API.
     */
    public function get($url, $params = array(), $noAuth = false)
    {
        if (!$noAuth && $this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for GET.');
            }
        } elseif (!$noAuth && $this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        $url .= '?' . http_build_query($params);

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true);

        if (!$noAuth) {
            $opts[CURLOPT_HTTPHEADER][] = self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken();
        }

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] == 404) {
            throw new Lingo24ApiNotFoundException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 200
            && $info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }

        return $response;
    }

    /**
     * Performs a GET request to the API using cURL and process the response as JSON.
     *
     * @param string  $url    The API URL to make a GET request to.
     * @param array   $params An array of parameters to be passed with the GET request.
     * @param boolean $noAuth Skip authentication for this request.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function getJson($url, $params = array(), $noAuth = false)
    {
        $response = $this->get($url, $params, $noAuth);

        return json_decode($response);
    }

    /**
     * Performs a POST request to the API using cURL.
     *
     * @param string  $url     The API URL to make a POST request to.
     * @param array   $params  An array of parameters to be passed with the POST request.
     * @param boolean $noAuth  Skip authentication for this request.
     * @param array   $options Additional options for the request.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function post($url, $params = array(), $noAuth = false, $options = array())
    {
        if (!$noAuth && $this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for POST.');
            }
        } elseif (!$noAuth && $this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        if (!isset($options[self::$OPTIONS_CONTENT_TYPE])) {
            $options[self::$OPTIONS_CONTENT_TYPE] = 'application/x-www-form-urlencoded';
        }

        $data = '';
        if ($options[self::$OPTIONS_CONTENT_TYPE] == 'multipart/form-data') {
            $boundary = uniqid();
            $options[self::$OPTIONS_CONTENT_TYPE] .= "; boundary=$boundary";
            foreach ($params as $key => $value) {
                $data .= "--{$boundary}\nContent-Disposition: form-data; name=\"$key\"; filename=\"$key\"\r\n"
                       . "Content-type: " . $value[self::$OPTIONS_CONTENT_TYPE] . "\r\n\r\n"
                       . $value['body'] . "\r\n";
            }
            $data .= "--{$boundary}--\r\n";
        } elseif ($options[self::$OPTIONS_CONTENT_TYPE] == 'application/octet-stream') {
            $data = $params;
        } else {
            $data = http_build_query($params);
        }

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_POST           => true,
                      CURLOPT_HTTPHEADER     => array('Content-Type: ' . $options[self::$OPTIONS_CONTENT_TYPE]),
                      CURLOPT_POSTFIELDS     => $data,
                      CURLINFO_HEADER_OUT    => true);

        if (!$noAuth) {
            $opts[CURLOPT_HTTPHEADER][] = self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken();
        }

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 200
            && $info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }

        return json_decode($response);
    }

    /**
     * Performs a POST request to the API using cURL, sending a JSON payload.
     *
     * @param string $url     The API URL to make a POST request to.
     * @param array  $payload The object/array to be sent as a JSON string.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function postJson($url, $payload)
    {
        if ($this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for POST.');
            }
        } elseif ($this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        $json = json_encode($payload);

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_POST           => true,
                      CURLOPT_HTTPHEADER     =>
                          array('Content-Type: application/json',
                                'Content-Length: ' . strlen($json),
                              self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken()),
                      CURLOPT_POSTFIELDS     => $json,
                      CURLINFO_HEADER_OUT    => true);

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 200
            && $info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }

        return json_decode($response);
    }

    /**
     * Performs a PUT request to the API using cURL.
     *
     * @param string $url     The API URL to make a PUT request to.
     * @param array  $payload The object/array to be sent.
     * @param array  $options Additional options for the request.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function put($url, $payload, $options = array())
    {
        if ($this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for PUT.');
            }
        } elseif ($this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        $opts = array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => 'PUT',
            CURLOPT_HTTPHEADER     => array(
                'Content-Type: ' . $options[self::$OPTIONS_CONTENT_TYPE],
                self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken()
            ),
            CURLOPT_POSTFIELDS     => $payload,
            CURLINFO_HEADER_OUT    => true
        );

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 200
            && $info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }

        return json_decode($response);
    }

    /**
     * Performs a PUT request to the API using cURL, sending a JSON payload.
     *
     * @param string $url     The API URL to make a PUT request to.
     * @param array  $payload The object/array to be sent as a JSON string.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function putJson($url, $payload)
    {
        if ($this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for PUT.');
            }
        } elseif ($this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        $json = json_encode($payload);

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_CUSTOMREQUEST  => 'PUT',
                      CURLOPT_HTTPHEADER     =>
                      array('Content-Type: application/json',
                            'Content-Length: ' . strlen($json),
                          self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken()),
                      CURLOPT_POSTFIELDS     => $json,
                      CURLINFO_HEADER_OUT    => true);

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 200 && $info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }

        return json_decode($response);
    }

    /**
     * Performs a DELETE request to the API using cURL.
     *
     * @param string $url The API URL to make a DELETE request to.
     */
    public function delete($url)
    {
        if ($this->oauth2 != null) {
            if (!$this->authorise($this->oauth2)) {
                throw new Lingo24ApiAuthorisationException('Authorisation failed for DELETE.');
            }
        } elseif ($this->oauth2 == null) {
            throw new Lingo24ApiAuthorisationException();
        }

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_CUSTOMREQUEST  => 'DELETE',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_HTTPHEADER     => array(
                          self::$HTTP_HEADER_AUTHORIZATION_BEARER . $this->oauth2->getAccessToken()
                      ));

        list($response, $info) = $this->curl($opts);

        if ($info[self::$CURLINFO_HTTP_CODE_STRING] == 401) {
            throw new Lingo24ApiAuthorisationException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] == 404) {
            throw new Lingo24ApiNotFoundException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] == 405) {
            throw new Lingo24ApiNotAllowedException($response);
        } elseif ($info[self::$CURLINFO_HTTP_CODE_STRING] != 204) {
            throw new Lingo24ApiException(
                "Error response; code: ". $info[self::$CURLINFO_HTTP_CODE_STRING] . ", content: " . $response
            );
        }
    }

    /**
     * @see AbstractOAuth2Client::authorise()
     */
    public function authorize($authCodeOrOAuth2)
    {
        return $this->authorise($authCodeOrOAuth2);
    }

    /**
     * Fetches a resource from the API.
     *
     * @param string $href The resource URI.
     * @param Class $type The resource type.
     *
     * @return Resource A resource object of type $type.
     */
    public function getResource($href, $type)
    {
        $result = $this->getJson($href);

        return new $type($result);
    }

    /**
     * Deletes a resource from the API.
     *
     * @param Resource $resource The resource to delete.
     */
    public function deleteResource(Resource $resource)
    {
        $this->delete($resource->getSelf());
    }

    /**
     * Getter function for the OAuth2 model object, this can be saved and used to create new clients.
     *
     * @return OAuth2 The client's OAuth2 model object.
     */
    public function getOAuth2()
    {
        return $this->oauth2;
    }

    /**
     * Getter method of the current API environment.
     *
     * @return string The current environment.
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * Getter method for the custom base URL.
     *
     * @return string The custom base URL.
     */
    public function getCustomBaseUrl()
    {
        return $this->customBaseUrl;
    }

    /**
     * Return the API's base url.
     *
     * @return string The base url.
     */
    abstract function getBaseUrl();

    /**
     * Return the API OAuth2 authorise url.
     *
     * @return string The Ease Oauth2 authorise url.
     */
    abstract function getAuthUrl();
}
