<?php

namespace Lingo24\API;

use Lingo24\API\Model\Domain;
use Lingo24\API\Model\File;
use Lingo24\API\Model\Job;
use Lingo24\API\Model\TranslationLocale;
use Lingo24\API\Model\Project;
use Lingo24\API\Model\Service;

/**
 * Project creation Wizard for the Lingo24 Business Documents API. Allows a project to be built up at the client side,
 * adding source files and locales, then have the project and jobs (for each file/locale combination) created.
 */
class ProjectWizard
{
    /**
     * @var The project name.
     */
    private $name;

    /**
     * @var The domain selected for the jobs.
     */
    private $domain;

    /**
     * @var The service selected for the jobs.
     */
    private $service;

    /**
     * @var The source locale for the jobs.
     */
    private $sourceLocale;

    /**
     * @var An array of source files.
     */
    private $files = array();

    /**
     * @var An array of target locales.
     */
    private $targetLocales = array();

    /**
     * @var array The project's tags.
     */
    private $tags = array();

    /**
     * Constructor for the Wizard, requires a project name.
     *
     * @param string $name The project name.
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Getter method for the project name.
     *
     * @return string The project name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Getter method for the domain.
     *
     * @return Domain $domain The domain selected for the jobs.
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Setter method for the domain.
     *
     * @param Domain The domain service for the jobs.
     *
     * @return ProjectWizard The project wizard.
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Getter method for the service.
     *
     * @return Service The service selected for the jobs.
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Setter method for the service.
     *
     * @param Service $service The selected service for the jobs.
     *
     * @return ProjectWizard The project wizard.
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Getter method for the source locale.
     *
     * @return TranslationLocale The source locale selected for the jobs.
     */
    public function getSourceLocale()
    {
        return $this->sourceLocale;
    }

    /**
     * Setter method for the source locale.
     *
     * @param TranslationLocale $sourceLocale The selected source locale for the jobs.
     *
     * @return ProjectWizard The project wizard.
     */
    public function setSourceLocale($sourceLocale)
    {
        $this->sourceLocale = $sourceLocale;

        return $this;
    }

    /**
     * Add a file to the list of source files.
     *
     * @param string $path The locale filepath.
     * @param string $name The file's name.
     *
     * @return ProjectWizard The project wizard.
     */
    public function addFile($path, $name)
    {
        $this->files[$path] = $name;

        return $this;
    }

    /**
     * Getter method for the files list.
     *
     * @return array() The list of filenames added.
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add a target locale to the list.
     *
     * @param TranslationLocale $targetLocale The target locale.
     *
     * @return \Lingo24\API\ProjectWizard
     */
    public function addTargetLocale(TranslationLocale $targetLocale)
    {
        $this->targetLocales[$targetLocale->getId()] = $targetLocale;

        return $this;
    }

    /**
     * Getter method for the target locales list.
     *
     * @return array() The list of target locales.
     */
    public function getTargetLocales()
    {
        $locales = array();
        foreach ($this->targetLocales as $id => $locale) {
            $locales[] = $locale;
        }

        return $locales;
    }

    /**
     * Getter methods for the project's tags.
     *
     * @return array An array of tags.
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Setter method for the project's tags.
     *
     * @param $tags array A new list of tags.
     *
     * @return ProjectWizard The project wizard.
     */
    public function setTags($tags)
    {
        if (is_array($tags)) {
            $this->tags = $tags;
        }

        return $this;
    }

    /**
     * Adds a tag to the project.
     *
     * @param $tag String The tag to add.
     *
     * @return ProjectWizard The project wizard.
     */
    public function addTag($tag)
    {
        if (!in_array($tag, $this->tags)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    /**
     * Create the project via the API, a job is created for each source file / target language combination.
     *
     * @param Docs $docs An Business Documents API client.
     *
     * @return Project The newly created project.
     */
    public function create(Docs $docs)
    {
        $projectObject = new \stdClass();
        $projectObject->name = $this->name;
        $projectObject->tags = $this->tags;

        $project = $docs->createProject(new Project($projectObject));

        foreach ($this->files as $filepath => $filename) {
            $file = $docs->createFile($filename, $filepath);

            foreach ($this->targetLocales as $targetLocale) {
                $job = new Job($project);
                $job->setService($this->service);
                $job->setSourceLocale($this->sourceLocale);
                $job->setTargetLocale($targetLocale);
                $job->setSourceFile($file);

                $docs->createJob($job);
            }
        }

        return $project;
    }
}
