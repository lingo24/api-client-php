<?php

namespace Lingo24\API;

/**
 * Generic API client functions used for different API services.
 */
abstract class AbstractClient
{
    /**
     * Execute a cURL request using the supplied options.
     *
     * @param array $opts The cURL options to be used.
     *
     * @return array Contains the cURL info and response.
     */
    public function curl($opts)
    {
        $curl = curl_init();

        foreach ($opts as $opt => $value) {
            curl_setopt($curl, $opt, $value);
        }

        $response = curl_exec($curl);
        $info     = curl_getinfo($curl);

        curl_close($curl);

        return array($response, $info);
    }
}
