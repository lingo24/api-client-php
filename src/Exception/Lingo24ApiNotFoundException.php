<?php

namespace Lingo24\API\Exception;

/**
 * Exception class to be thrown by Lingo24 API clients when there is a 404 error. Throwing an exception allows the
 * client to handle the error as required (e.g. often just returning null, but sometimes other error handling is
 * required).
 */
class Lingo24ApiNotFoundException extends Lingo24ApiException
{
}
