<?php

namespace Lingo24\API\Exception;

/**
 * Exception class for exceptions thrown by Lingo24 API clients.
 */
class Lingo24ApiException extends \Exception
{
}
