<?php

namespace Lingo24\API\Exception;

/**
 * Exception class for exceptions thrown by Lingo24 API clients when there is an authorisation error, this is usually
 * an indication that the client needs to re-authorise.
 */
class Lingo24ApiAuthorisationException extends Lingo24ApiException
{
    function __construct()
    {
        parent::__construct('API client has not been authorised.');
    }
}
