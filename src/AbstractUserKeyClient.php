<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiAuthorisationException;
use Lingo24\API\Exception\Lingo24ApiException;

/**
 * Generic API client functions for API services using user key authentication.
 */
abstract class AbstractUserKeyClient extends AbstractClient
{
    /**
     * @var String The user's 3 Scale API key.
     */
    private $userKey;

    /**
     * @var string Custom base url, if set.
     */
    private $customBaseUrl;

    /**
     * Performs a GET request to the API using cURL. The user key is prepended to the input parameters provided.
     *
     * @param string $url    The API URL to make a GET request to.
     * @param array  $params An array of parameters to be passed with the GET request.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function get($url, $params)
    {
        $params = array('user_key' => $this->getUserKey()) + $params;

        $url .= '?' . http_build_query($params);

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true);

        list($response, $info) = $this->curl($opts);

        if ($info['http_code'] != 200) {
            throw new Lingo24ApiException($response);
        }

        return json_decode($response);
    }

    /**
     * Performs a POST request to the API using cURL. The user key is prepended to the input parameters provided.
     *
     * @param string $url    The API URL to make a POST request to.
     * @param array  $params An array of parameters to be passed with the POST request.
     *
     * @return Object The object returned by the API (parsed from JSON).
     */
    public function post($url, $params)
    {
        $params = array('user_key' => $this->getUserKey()) + $params;

        $opts = array(CURLOPT_URL            => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_POST           => true,
                      CURLOPT_HTTPHEADER     => array('Content-Type: application/x-www-form-urlencoded'),
                      CURLOPT_POSTFIELDS     => http_build_query($params),
                      CURLINFO_HEADER_OUT    => true);

        list($response, $info) = $this->curl($opts);

        if ($info['http_code'] != 200) {
            throw new Lingo24ApiException($response);
        }

        return json_decode($response);
    }

    /**
     * Setter function for the user key.
     *
     * @param string $userKey The user's 3 Scale API key.
     */
    public function setUserKey($userKey)
    {
        $this->userKey = $userKey;
    }

    /**
     * Getter function for the user key.
     *
     * @return string The user's 3 Scale API key.
     */
    public function getUserKey()
    {
        return $this->userKey;
    }


    /**
     * Setter function for the custom base URL.
     *
     * @param string $customBaseUrl The custom base URL.
     */
    public function setCustomBaseUrl($customBaseUrl)
    {
        $this->customBaseUrl = $customBaseUrl;
    }

    /**
     * Getter method for the custom base URL.
     *
     * @return string The custom base URL.
     */
    public function getCustomBaseUrl()
    {
        return $this->customBaseUrl;
    }
}
