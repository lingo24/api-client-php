# Lingo24 PHP Library for the [Lingo24 Translation API](https://developer.lingo24.com)

We love languages at Lingo24. Every day we work hard to translate documents, websites, tools and products, helping
people all over the world communicate with one another.

Translating your content can be challenging, but we aim to make it very simple through easy to use tools, friendly
people, and great technology.

In terms of translation, **[Lingo24](http://www.lingo24.com)** offers a range of professional human-translation and
premium machine translation services, all of which can accessed via our translation APIs.

This is a PHP interface to make using our APIs simpler.

## Installation & Requirements

Installing the Lingo24 API client is easy using [Composer](https://getcomposer.org/), simply include the library as a
dependency in your configuration file:

```javascript
"require": {
    "lingo24/api-client": "=2.*"
}
```

## Questions, Comments, Complaints, Compliments?

If you have questions or comments and would like to reach us directly, please feel free to do so at the following
outlets. We love hearing from developers!

* Email: api [at] lingo24 dot com
* Forum: [Lingo24 Developer Portal](https://developer.lingo24.com/forum)

If you come across any issues with the library, please file them on the project’s
[Bitbucket issue tracker](https://bitbucket.org/lingo24/api-client-php/issues).

## Accessing Our Premium Machine Translation API
To get started with the API you first have to authenticate. You do this by using the authentication information you
received when you signed up on the [Lingo24 Developer Portal](https://developer.lingo24.com).

```php
$lingo24 = new \Lingo24\API\MT('your_user_key')
```

If you do not have an _user_key_ please go to our [plans](https://developer.lingo24.com/plans) page to sign up for one
of our access plans. This includes our free Premium Machine Translation _Taster_ plan, with free access to translate
100,000 between English, French and Spanish.

Once you have done this you are free to use the methods exposed via the API wrapper to translate text, fetch source
languages and fetch target languages. You can also view the methods available on the online
[Lingo24 API documentation](https://developer.lingo24.com/premium-machine-translation-api).

## Accessing Our Business Documents API
To get started with the API you must first contact us to create a customer account linked to your
[Lingo24 Developer Portal](https://developer.lingo24.com) account. Authentication with the Business Documents API is
performed using OAuth2. To start using API functionality your users will need to authenticate, your application should
redirect users to our [Ease Client Portal](https://ease.lingo24.com/ui/oauth2/authorize), passing the request type
(code), client id and redirect URI as POST parameters. Once authorised the Ease portal will redirect users and provide
an authorisation code. The authorisation code can then used to create the library.

```php
$lingo24Docs = new \Lingo24\API\Docs('your_client_id', 'your_client_secret', 'your_application_url', 'authorisation code');
```

Once you have done this you are free to use the methods exposed via the API wrapper to create and manage jobs and
projects. You can view the methods available on the online
[Lingo24 API documentation](https://developer.lingo24.com/business-translation-api).

### Project Wizard
The API client library provides a "Project Wizard" as a convenient way of scripting project and job creation. The Wizard
allows you to add multiple source files and target locales, then create jobs for each combination.

```php
$lingo24Project = (new \Lingo24\API\ProjectWizard('My Project'))
    ->setDomain($lingo24Domain)
    ->setService($lingo24Service)
    ->setSourceLocale($lingo24SourceLocale)
    ->addFile('/tmp/file1.txt', 'My First File')
    ->addFile('/tmp/file2.txt', 'My Second File')
    ->addTargetLocale($lingo24TargetLocale1)
    ->addTargetLocale($lingo24TargetLocale2)
    ->create($lingo24Docs);
```

The example above will create a project containing four jobs, once for each of the file / target locale combinations.
Once the project has been created it can be used with the Business Documents API library as normal, e.g.

```php
$lingo24Docs->confirmProject($lingo24Project);
```