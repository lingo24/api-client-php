<?php

namespace Lingo24\API;

/**
 * Test the abstract API client.
 */
class ClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the curl() method. We're just hitting localhost as an example, but don't really care what comes back.
     */
    public function testCurl()
    {
        $client = new TestClient();

        $opts = array(CURLOPT_URL => 'http://localhost');

        list($response, $info) = $client->curl($opts);

        $this->assertNotNull($response);
        $this->assertNotNull($info);
    }
}
