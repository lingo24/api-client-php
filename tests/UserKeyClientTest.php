<?php

namespace Lingo24\API;

/**
 * Test class for the API user key client function.
 */
class UserKeyClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $URL      = "http://localhost";
    private static $USER_KEY = "userKey";

    /**
     * @var Client Instance of the Client class to use in tests.
     */
    private $client;

    /**
     * Instantiates a mock of the TestClient class to test the User key client abstract class.
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    public function setUp()
    {
        $this->client = \Mockery::mock('Lingo24\API\TestUserKeyClient[curl]');
    }

    /**
     * Test the get function.
     */
    public function testGet()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->get(self::$URL, array());

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the get function when an error is returned.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array(null, array('http_code' => '500')));

        $this->client->get(self::$URL, array());
    }

    /**
     * Test the post function.
     */
    public function testPost()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->post(self::$URL, array());

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the post function when an error is returned.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testPostError()
    {
        $this->client
        ->shouldReceive('curl')
        ->andReturn(array(null, array('http_code' => '500')));

        $this->client->post(self::$URL, array());
    }

    /**
     * Test the user key setter and getter methods by saving and retrieving a user key.
     */
    public function testUserKeySetterGetter()
    {
        $this->client->setUserKey(self::$USER_KEY);
        $this->assertEquals(self::$USER_KEY, $this->client->getUserKey());
    }
}
