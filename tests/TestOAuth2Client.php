<?php

namespace Lingo24\API;

/**
 * Implementation of the abstract OAuth2 Client class to use in tests.
 */
class TestOAuth2Client extends AbstractOAuth2Client
{
    /**
     * @var string $BASE_URL_DEMO The base Business Documents API demo environment url.
     */
    public static $BASE_URL_DEMO = 'https://demo';

    /**
     * @var string $BASE_URL The base Business Documents API url.
     */
    public static $BASE_URL_LIVE = 'https://live';

    /**
     * @var string $AUTH_URL_DEMO The Ease demo environment OAuth2 authorisation url.
     */
    public static $AUTH_URL_DEMO = 'https://live/authorize';

    /**
     * @var string $AUTH_URL_LIVE The Ease OAuth2 authorisation url.
     */
    public static $AUTH_URL_LIVE = 'https://demo/authorize';

    /**
     * Return the API's base url.
     *
     * @return string The base url.
     */
    public function getBaseUrl()
    {
        if ($this->getCustomBaseUrl() != null) {
            return $this->getCustomBaseUrl();
        }

        if ($this->getEnv() == self::$DEMO_ENV) {
            return self::$BASE_URL_DEMO;
        }

        return self::$BASE_URL_LIVE;
    }

    /**
     * Return the Ease OAuth2 authorise url.
     *
     * @return string The Ease Oauth2 authorise url.
     */
    public function getAuthUrl()
    {
        if ($this->getEnv() == self::$DEMO_ENV) {
            return self::$AUTH_URL_DEMO;
        }

        return self::$AUTH_URL_LIVE;
    }
}
