<?php

namespace Lingo24\API;

use Lingo24\API\Model\Domain;
use Lingo24\API\Model\File;
use Lingo24\API\Model\Job;
use Lingo24\API\Model\TranslationLocale;
use Lingo24\API\Model\Project;
use Lingo24\API\Model\Service;

/**
 * Test class for the project wizard.
 */
class ProjectWizardTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $PROJECT_NAME          = "projectName";
    private static $DOMAIN_JSON           = '{"id": 1, "name": "domain"}';
    private static $SERVICE_JSON          = '{"id": 2, "name": "service", "description": "serviceDescription"}';
    private static $SOURCE_LOCALE_JSON    = '{"id": 3, "language": "sourceLanguage", "country": "sourceCountry"}';
    private static $TARGET_LOCALE_JSON_1  = '{"id": 4, "language": "targetLanguage1", "country": "targetCountry2"}';
    private static $TARGET_LOCALE_JSON_2  = '{"id": 5, "language": "targetLanguage2", "country": "targetCountry2"}';
    private static $FILENAME_1            = "filename1";
    private static $FILENAME_2            = "filename2";
    private static $FILEPATH_1            = "/tmp/one.txt";
    private static $FILEPATH_2            = "/tmp/two.txt";
    private static $CLIENT_ID             = "clientId";
    private static $CLIENT_SECRET         = "clientSecret";
    private static $REDIRECT_URI          = "http://localhost";
    private static $PROJECT_JSON          = <<<END
{"id": 6, "name": "projectName", "created": 1, "projectStatus": "CREATED", "tags": ["Tag One", "Tag Two"]}
END;
    private static $FILE_JSON_1           = '{"id": 7, "name": "filename1", "type": "SOURCE"}';
    private static $FILE_JSON_2           = '{"id": 8, "name": "filename2", "type": "SOURCE"}';
    private static $TAG_1                 = 'Tag One';
    private static $TAG_2                 = 'Tag Two';
    private static $TAGS                  = array('Tag One', 'Tag Two');

    private $domain;
    private $service;
    private $sourceLocale;
    private $targetLocale1;
    private $targetLocale2;
    private $docs;
    private $project;
    private $file1;
    private $file2;

    /**
     * Create the test data objects.
     */
    public function setUp()
    {
        $this->domain          = new Domain(json_decode(self::$DOMAIN_JSON));
        $this->service         = new Service(json_decode(self::$SERVICE_JSON));
        $this->sourceLocale    = new TranslationLocale(json_decode(self::$SOURCE_LOCALE_JSON));
        $this->targetLocale1   = new TranslationLocale(json_decode(self::$TARGET_LOCALE_JSON_1));
        $this->targetLocale2   = new TranslationLocale(json_decode(self::$TARGET_LOCALE_JSON_2));
        $this->project         = new Project(json_decode(self::$PROJECT_JSON));
        $this->file1           = new File(json_decode(self::$FILE_JSON_1));
        $this->file2           = new File(json_decode(self::$FILE_JSON_2));

        $this->docs = \Mockery::mock(
            'Lingo24\API\Docs[createProject,createFile,createJob]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );
    }

    /**
     * Test the wizard's functionality.
     */
    public function testWizard()
    {
        $wizard = (new ProjectWizard(self::$PROJECT_NAME))
            ->setDomain($this->domain)
            ->setService($this->service)
            ->setSourceLocale($this->sourceLocale)
            ->addTargetLocale($this->targetLocale1)
            ->addTargetLocale($this->targetLocale2)
            ->addFile(self::$FILEPATH_1, self::$FILENAME_1)
            ->addFile(self::$FILEPATH_2, self::$FILENAME_2)
            ->setTags(array(self::$TAG_1))
            ->addTag(self::$TAG_2);

        $this->assertEquals($wizard->getName(), self::$PROJECT_NAME);
        $this->assertEquals($wizard->getDomain(), $this->domain);
        $this->assertEquals($wizard->getService(), $this->service);
        $this->assertEquals($wizard->getSourceLocale(), $this->sourceLocale);
        $this->assertEquals(count($wizard->getFiles()), 2);
        $this->assertEquals($wizard->getFiles()[self::$FILEPATH_1], self::$FILENAME_1);
        $this->assertEquals($wizard->getFiles()[self::$FILEPATH_2], self::$FILENAME_2);
        $this->assertEquals(count($wizard->getTargetLocales()), 2);
        $this->assertEquals($wizard->getTargetLocales()[0], $this->targetLocale1);
        $this->assertEquals($wizard->getTargetLocales()[1], $this->targetLocale2);
        $this->assertEquals($wizard->getTags(), self::$TAGS);

        $this->docs
            ->shouldReceive('createProject')
            ->andReturn($this->project);

        $this->docs
            ->shouldReceive('createFile')
            ->with(self::$FILENAME_1, self::$FILEPATH_1)
            ->andReturn($this->file1);

        $this->docs
            ->shouldReceive('createFile')
            ->with(self::$FILENAME_2, self::$FILEPATH_2)
            ->andReturn($this->file2);

        $this->docs
            ->shouldReceive('createJob')
            ->twice();

        $newProject = $wizard->create($this->docs);

        $this->assertEquals($newProject, $this->project);
    }
}
