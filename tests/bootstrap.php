<?php

/**
 * Bootstrap for PHPUnit tests. Adds any classes with the Lingo24\API namespace and within the tests directory to the
 * autoloader.
 */

$loader = require __DIR__ . "/../vendor/autoload.php";
$loader->addPsr4('Lingo24\\API\\', __DIR__);
