<?php

namespace Lingo24\API;

/**
 * Implementation of the abstract User Key Client class to use in tests.
 */
class TestUserKeyClient extends AbstractUserKeyClient
{
}
