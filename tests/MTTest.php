<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiException;

/**
 * Test class for the MT API client.
 */
class MTTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $USER_KEY          = "userKey";
    private static $SOURCE            = "source";
    private static $TARGET            = "target";
    private static $SOURCES_JSON      = '{"source_langs": [["code", "language"]], "success": "true"}';
    private static $TARGET_JSON       = '{"target_langs": [["code", "language"]], "success": "true"}';
    private static $TRANSLATION_JSON  = '{"translation": "translation", "success": "true"}';
    private static $UNSUCCESSFUL_JSON = '{"success": "false", "errors": ["error"]}';
    private static $BASE_URL          = "http://localhost";

    /**
     * @var MT Instance of the MT API client to use in tests.
     */
    private $mt;

    /**
     * Create an instance of the MT API client as a partial mock, leaving the get() and post() functions unimplemented.
     *
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    public function setUp()
    {
        $this->mt = \Mockery::mock('Lingo24\API\MT[get,post]', array(self::$USER_KEY));
    }

    /**
     * Test a call to get a list of source languages from the API.
     */
    public function testGetSources()
    {
        $this->mt
            ->shouldReceive('get')
            ->andReturn(json_decode(self::$SOURCES_JSON));

        $languages = $this->mt->getSources();

        $this->assertEquals(1, count($languages));
        $this->assertEquals('language', $languages['code']);
    }

    /**
     * Test a call to get a list of source languages from the API, with a supplied target.
     */
    public function testGetSourcesTarget()
    {
        $this->mt
            ->shouldReceive('get')
            ->andReturn(json_decode(self::$SOURCES_JSON));

        $languages = $this->mt->getSources(self::$TARGET);

        $this->assertEquals(1, count($languages));
        $this->assertEquals('language', $languages['code']);
    }

    /**
     * Test an unsuccessful call to get a list of source languages from the API.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetSourcesUnsuccessful()
    {
        $this->mt
            ->shouldReceive('get')
            ->andReturn(array('success' => 'false'));

        $this->mt->getSources();
    }

    /**
     * Test the error handling during a call to get a list of source languages from the API.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetSourcesException()
    {
        $this->mt
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiException());

        $this->mt->getSources();
    }

    /**
     * Test a call to get a list of target languages from the API.
     */
    public function testGetTargets()
    {
        $this->mt
            ->shouldReceive('get')
            ->andReturn(json_decode(self::$TARGET_JSON));

        $languages = $this->mt->getTargets();

        $this->assertEquals(1, count($languages));
        $this->assertEquals('language', $languages['code']);
    }

    /**
     * Test a call to get a list of target languages from the API, with a supplied source.
     */
    public function testGetTargetsSource()
    {
        $this->mt
        ->shouldReceive('get')
        ->andReturn(json_decode(self::$TARGET_JSON));

        $languages = $this->mt->getTargets(self::$SOURCE);

        $this->assertEquals(1, count($languages));
        $this->assertEquals('language', $languages['code']);
    }

    /**
     * Test an unsuccessful call to get a list of target languages from the API.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetTargetUnsuccessful()
    {
        $this->mt
            ->shouldReceive('get')
            ->andReturn(array('success' => 'false'));

        $this->mt->getTargets();
    }

    /**
     * Test the error handling during a call to get a list of target languages from the API.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetTargetsError()
    {
        $this->mt
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiException());

        $this->mt->getTargets();
    }

    /**
     * Test a call to translate text using a source and target language.
     */
    public function testTranslate()
    {
        $this->mt
            ->shouldReceive('post')
            ->andReturn(json_decode(self::$TRANSLATION_JSON));

        $translation = $this->mt->translate('source', 'target', 'input');

        $this->assertEquals('translation', $translation);
    }

    /**
     * Test an unsuccessful call to translate text using a source and target language.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testTranslateUnsuccessful()
    {
        $this->mt
        ->shouldReceive('post')
        ->andReturn(json_decode(self::$UNSUCCESSFUL_JSON));

        $this->mt->translate('source', 'target', 'input');
    }

    /**
     * Test the error handing during a call to translate text using a source and target language.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testTranslateError()
    {
        $this->mt
            ->shouldReceive('post')
            ->andThrow(new Lingo24ApiException());

        $this->mt->translate('source', 'target', 'input');
    }

    /**
     * Test a call to translate text using a specific engine id.
     */
    public function testTranslateWithEngine()
    {
        $this->mt
            ->shouldReceive('post')
            ->andReturn(json_decode(self::$TRANSLATION_JSON));

        $translation = $this->mt->translate(1, 'input');

        $this->assertEquals('translation', $translation);
    }

    /**
     * Test an unsuccessful call to translate text using a specific engine id.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testTranslateWithEngineUnsuccessful()
    {
        $this->mt
            ->shouldReceive('post')
            ->andReturn(json_decode(self::$UNSUCCESSFUL_JSON));

        $this->mt->translate(1, 'input');
    }

    /**
     * Test the error handing during a call to translate text using a specific engine id.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testTranslateWithEngineError()
    {
        $this->mt
        ->shouldReceive('post')
        ->andThrow(new Lingo24ApiException());

        $this->mt->translate(1, 'input');
    }

    /**
     * Test the custom base URL functionality.
     */
    public function testCustomBaseUrl()
    {
        $this->mt = \Mockery::mock('Lingo24\API\MT[get,post]', array(self::$USER_KEY, self::$BASE_URL));
        $this->assertEquals(self::$BASE_URL, $this->mt->getBaseUrl());
    }
}
