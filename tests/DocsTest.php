<?php

namespace Lingo24\API;

use Lingo24\API\Exception\Lingo24ApiException;
use Lingo24\API\Exception\Lingo24ApiNotFoundException;
use Lingo24\API\Model\File;
use Lingo24\API\Model\FileType;
use Lingo24\API\Model\Job;
use Lingo24\API\Model\TranslationLocale;
use Lingo24\API\Model\Project;
use Lingo24\API\Model\ProjectPrice;
use Lingo24\API\Model\ProjectStatus;
use Lingo24\API\Model\Service;

/**
 * Test class for the MT API client.
 */
class DocsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $CLIENT_ID     = "clientId";
    private static $CLIENT_SECRET = "clientSecret";
    private static $REDIRECT_URI  = "http://localhost";
    private static $AUTH_CODE     = "authCode";
    private static $ACCESS_TOKEN  = "accessToken";
    private static $REFRESH_TOKEN = "refreshToken";

    private static $SERVICE_ID          = 1;
    private static $UNKNOWN_SERVICE_ID  = 2;
    private static $SERVICE_NAME        = "serviceName";
    private static $SERVICE_DESCRIPTION = "serviceDescription";
    private static $SERVICE_JSON        =  '{"id": 1, "name": "serviceName", "description": "serviceDescription"}';
    private static $SERVICES_JSON       = <<<END
{"page": {"totalElements": 1}, "content": [{"id": 1, "name": "serviceName", "description": "serviceDescription"}]}
END;

    private static $DOMAIN_ID         = 3;
    private static $UNKNOWN_DOMAIN_ID = 4;
    private static $DOMAIN_NAME       = "domainName";
    private static $DOMAINS_JSON      = '{"page": {"totalElements": 1}, "content": [{"id": 3, "name": "domainName"}]}';

    private static $LOCALE_ID_1       = 5;
    private static $LOCALE_ID_2       = 6;
    private static $UNKNOWN_LOCALE_ID = 7;
    private static $LOCALE_LANGUAGE_1 = "bb";
    private static $LOCALE_LANGUAGE_2 = "aa";
    private static $LOCALE_COUNTRY_1  = "BB";
    private static $LOCALE_COUNTRY_2  = "AA";
    private static $LOCALE_JSON_1     =
        '{"id": 5, "language": "bb", "country": "BB"}';
    private static $LOCALE_JSON_2     =
        '{"id": 6, "language": "aa", "country": "AA"}';
    private static $LOCALES_JSON      = <<<END
{
    "page":    {"totalElements": 1},
    "content": [{"id": 5, "language": "bb", "country": "BB"}, {"id": 6, "language": "aa", "country": "AA"}]
}
END;

    private static $PROJECT_ID               = 8;
    private static $PROJECT_NAME             = "projectName";
    private static $PROJECT_CREATED          = 1;
    private static $PROJECT_STATUS           = 'CREATED';
    private static $PROJECT_QUOTED_STATUS    = 'QUOTED';
    private static $PROJECT_JSON             = <<<END
{
    "id": 8,
    "name": "projectName",
    "created": 1,
    "projectStatus": "CREATED",
    "tags": ["Tag One"],
    "links": [
        {"rel": "self", "href": "projects/8"},
        {"rel": "jobs", "href": "projects/8/jobs"},
        {"rel": "price", "href": "projects/8/price"},
        {"rel": "charges", "href": "projects/8/charges"}
    ]
}
END;
    private static $IN_PROGRESS_PROJECT_JSON =
        '{"id": 8, "name": "projectName", "created": 1, "projectStatus": "IN_PROGRESS",
   "links": [{"rel": "self", "href": "projects/8"}]}';
    private static $QUOTED_PROJECT_JSON      =
        '{"id": 8, "name": "projectName", "created": 1, "projectStatus": "QUOTED",
    "links": [{"rel": "self", "href": "projects/8"}]}';
    private static $PROJECTS_JSON            = <<<END
{"page": {"totalElements": 1}, "content": [{"id": 8, "name": "projectName", "created": 1, "projectStatus": "CREATED"}]}
END;
    private static $PROJECT_PRICE_JSON       = <<<END
{
    "currencyCode": "GBP",
    "totalWoVatWDiscount": 0.29,
    "totalWoVatWoDiscount": 0.29,
    "totalWVatWDiscount": 0.35,
    "totalWVatWoDiscount": 0.35
}
END;
    private static $CHARGE_TITLE               = 'title';
    private static $CHARGE_VALUE               = 0.29;
    private static $PROJECT_CHARGES_JSON       = <<<END
{
    "content": [
        {
            "title": "title",
            "value": 0.29
        }
    ],
    "links": [
        {"rel": "project", "href": "projects/8"}
    ]
}
END;

    private static $FILE_ID          = 9;
    private static $UNKNOWN_FILE_ID  = 10;
    private static $FILE_NAME        = "test.txt";
    private static $FILE_TYPE        = "SOURCE";
    private static $FILE_JSON        = <<<END
{
    "id": 9,
    "name": "test.txt",
    "type": "SOURCE",
    "links": [{"rel": "self", "href": "files/9"}, {"rel": "content", "href": "files/9/content"}]
}
END;
    private static $FILE_CONTENT     = "This is just a test.";

    private static $JOB_ID               = 11;
    private static $JOB_STATUS           = "QUOTED";
    private static $JOB_STATUS_STRING    = "Quoted";
    private static $JOB_SERVICE_ID       = 1;
    private static $JOB_SOURCE_LOCALE_ID = 5;
    private static $JOB_TARGET_LOCALE_ID = 6;
    private static $JOB_SOURCE_FILE_ID   = 9;
    private static $JOBS_JSON            = <<<END
{
    "content": [
        {
            "id": 11,
            "jobStatus": "QUOTED",
            "serviceId": 1,
            "sourceLocaleId": 5,
            "targetLocaleId": 6,
            "sourceFileId": 9,
            "projectId": 8,
            "links": [
                {"rel": "self", "href": "jobs/11"},
                {"rel": "project", "href": "projects/8"},
                {"rel": "service", "href": "services/1"},
                {"rel": "source-locale", "href": "locales/5"},
                {"rel": "target-locale", "href": "locales/6"},
                {"rel": "source-file", "href": "files/9"},
                {"rel": "metrics", "href": "jobs/11/metrics"},
                {"rel": "price", "href": "jobs/11/price"}
            ]
        }
    ]
}
END;
    private static $JOB_JSON             = <<<END
{
    "id": 11,
    "jobStatus": "QUOTED",
    "serviceId": 1,
    "sourceLocaleId": 5,
    "targetLocaleId": 6,
    "sourceFileId": 9,
    "projectId": 8,
    "links": [
        {"rel": "self", "href": "jobs/11"},
        {"rel": "project", "href": "projects/8"},
        {"rel": "service", "href": "services/1"},
        {"rel": "source-locale", "href": "locales/5"},
        {"rel": "target-locale", "href": "locales/6"},
        {"rel": "source-file", "href": "files/9"},
        {"rel": "metrics", "href": "jobs/11/metrics"},
        {"rel": "price", "href": "jobs/11/price"}
    ]
}
END;
    private static $IN_PROGRESS_JOB_JSON = '{"id": 11, "jobStatus": "IN_PROGRESS"}';
    private static $JOB_METRICS_JSON = <<<END
{
    "values": {
        "FUZZY_MATCH_75_84": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "REPETITION_75_84": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 5,
        "WHITE_SPACES": 0
        },
        "REPETITION_100": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "REPETITION_95_99": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "IN_CONTEXT_EXACT_MATCH": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "TOTAL": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 2,
        "WHITE_SPACES": 0
        },
        "NON_TRANSLATABLE": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "LEVERAGED_MATCH": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "FUZZY_MATCH_85_94": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "REPETITION_ICE": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "REPETITION_85_94": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "FUZZY_MATCH_95_99": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 0,
        "WHITE_SPACES": 0
        },
        "NO_MATCH": {
        "CHARACTERS": 0,
        "SEGMENTS": 0,
        "WORDS": 2,
        "WHITE_SPACES": 0
        }
    }
}
END;

    private static $CURRENCY                     = 'GBP';
    private static $TOTAL_WO_VAT_W_DISCOUNT      = 0.29;
    private static $TOTAL_WO_VAT_WO_DISCOUNT     = 0.29;
    private static $TOTAL_W_VAT_W_DISCOUNT       = 0.35;
    private static $TOTAL_W_VAT_WO_DISCOUNT      = 0.35;

    private static $JOB_PRICE_JSON = <<<END
{
    "currencyCode": "GBP",
    "totalWoVatWDiscount": 0.29,
    "totalWoVatWoDiscount": 0.29,
    "totalWVatWDiscount": 0.35,
    "totalWVatWoDiscount": 0.35
}
END;

    private static $TAGS_JSON = '{"page": {"totalElements": 1}, "content": ["Tag One", "Tag Two"]}';
    private static $TAG = 'Tag One';

    /**
     * @var Docs Instance of the Docs API client to use in tests.
     */
    private $docs;

    /**
     * Create an instance of the Docs API client as a partial mock, leaving the get() and post() functions
     * unimplemented.
     *
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    public function setUp()
    {
        $this->docs = \Mockery::mock(
            'Lingo24\API\Docs[get,post,put,postJson,putJson,delete]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );
    }

    /**
     * Test a call to the API OAuth2 access authorisation using an authorisation code.
     */
    public function testAuthoriseAuthCode()
    {
        $json = '{"access_token": "' . self::$ACCESS_TOKEN . '", "expires_in": 3600, '
              . '"refresh_token": "'. self::$REFRESH_TOKEN . '", "token_type": "Bearer"}';

        $this->docs
            ->shouldReceive('post')
            ->andReturn(json_decode($json));

        $result = $this->docs->authorise(self::$AUTH_CODE);

        $this->assertTrue($result);
        $this->assertEquals($this->docs->getOAuth2()->getAccessToken(), self::$ACCESS_TOKEN);
        $this->assertEquals($this->docs->getOAuth2()->getRefreshToken(), self::$REFRESH_TOKEN);
    }

    /**
     * Test a call to get services, firstly where they need to be fetched and then again to check they are cached.
     */
    public function testGetServices()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$SERVICES_JSON);

        $result = $this->docs->getServices();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getId(), self::$SERVICE_ID);
        $this->assertEquals($result[0]->getName(), self::$SERVICE_NAME);
        $this->assertEquals($result[0]->getDescription(), self::$SERVICE_DESCRIPTION);

        $cache = $this->docs->getServices();

        $this->assertEquals($cache, $result);
    }

    /**
     * Test a call to get a service.
     */
    public function testGetService()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$SERVICES_JSON);

        $result = $this->docs->getService(self::$SERVICE_ID);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$SERVICE_ID);
        $this->assertEquals($result->getName(), self::$SERVICE_NAME);
        $this->assertEquals($result->getDescription(), self::$SERVICE_DESCRIPTION);
    }

    /**
     * Test a call to get a unknown service.
     */
    public function testGetUnknownService()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$SERVICES_JSON);

        $result = $this->docs->getService(self::$UNKNOWN_SERVICE_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to get domains, firstly where they need to be fetched and then again to check they are cached.
     */
    public function testGetDomains()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$DOMAINS_JSON);

        $result = $this->docs->getDomains();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getId(), self::$DOMAIN_ID);
        $this->assertEquals($result[0]->getName(), self::$DOMAIN_NAME);

        $cache = $this->docs->getDomains();

        $this->assertEquals($cache, $result);
    }

    /**
     * Test a call to get a domain.
     */
    public function testGetDomain()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$DOMAINS_JSON);

        $result = $this->docs->getDomain(self::$DOMAIN_ID);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$DOMAIN_ID);
        $this->assertEquals($result->getName(), self::$DOMAIN_NAME);
    }

    /**
     * Test a call to get a unknown domain.
     */
    public function testGetUnknownDomain()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$DOMAINS_JSON);

        $result = $this->docs->getDomain(self::$UNKNOWN_DOMAIN_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to get locales, firstly where they need to be fetched and then again to check they are cached.
     * Check that the results are in the correct order.
     */
    public function testGetLocales()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$LOCALES_JSON);

        $result = $this->docs->getLocales();

        $this->assertEquals(count($result), 2);
        $this->assertEquals($result[0]->getId(), self::$LOCALE_ID_2);
        $this->assertEquals($result[0]->getLanguage(), self::$LOCALE_LANGUAGE_2);
        $this->assertEquals($result[0]->getCountry(), self::$LOCALE_COUNTRY_2);
        $this->assertEquals($result[1]->getId(), self::$LOCALE_ID_1);
        $this->assertEquals($result[1]->getLanguage(), self::$LOCALE_LANGUAGE_1);
        $this->assertEquals($result[1]->getCountry(), self::$LOCALE_COUNTRY_1);

        $cache = $this->docs->getLocales();

        $this->assertEquals($cache, $result);
    }

    /**
     * Test a call to get a locale.
     */
    public function testGetLocale()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$LOCALES_JSON);

        $result = $this->docs->getLocale(self::$LOCALE_ID_1);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$LOCALE_ID_1);
        $this->assertEquals($result->getLanguage(), self::$LOCALE_LANGUAGE_1);
        $this->assertEquals($result->getCountry(), self::$LOCALE_COUNTRY_1);
    }

    /**
     * Test a call to get a locale by locale string.
     */
    public function testGetLocaleByLocale()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$LOCALES_JSON);

        $result = $this->docs->getLocale(self::$LOCALE_LANGUAGE_1 . '_' . self::$LOCALE_COUNTRY_1);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$LOCALE_ID_1);
        $this->assertEquals($result->getLanguage(), self::$LOCALE_LANGUAGE_1);
        $this->assertEquals($result->getCountry(), self::$LOCALE_COUNTRY_1);
    }

    /**
     * Test a call to get a unknown locale.
     */
    public function testGetUnknownLocale()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$LOCALES_JSON);

        $result = $this->docs->getLocale(self::$UNKNOWN_LOCALE_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to get projects.
     */
    public function testGetProjects()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$PROJECTS_JSON);

        $result = $this->docs->getProjects();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getId(), self::$PROJECT_ID);
        $this->assertEquals($result[0]->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result[0]->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result[0]->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to get a project.
     */
    public function testGetProject()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$PROJECT_JSON);

        $result = $this->docs->getProject(self::$PROJECT_ID);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to get a unknown project.
     */
    public function testGetUnknownProject()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException(''));

        $result = $this->docs->getProject(self::$PROJECT_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to create a project.
     */
    public function testCreateProject()
    {
        $this->docs
        ->shouldReceive('postJson')
        ->andReturn(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->createProject(new Project(json_decode(self::$PROJECT_JSON)));

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
        $this->assertEquals($result->getTags(), array(self::$TAG));
    }

    /**
     * Test a call to create a project by name.
     */
    public function testCreateProjectByName()
    {
        $this->docs
            ->shouldReceive('postJson')
            ->andReturn(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->createProject(self::$PROJECT_NAME);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to update a project.
     */
    public function testUpdateProject()
    {
        $this->docs
            ->shouldReceive('putJson')
            ->andReturn(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->updateProject(new Project(json_decode(self::$PROJECT_JSON)));

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to update a project when no id is provided.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateProjectNoId()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $project->setId(null);

        $this->docs->updateProject($project);
    }

    /**
     * Test a call to quote a project.
     */
    public function testQuoteProject()
    {
        $this->docs
            ->shouldReceive('putJson')
            ->andReturn(json_decode(self::$QUOTED_PROJECT_JSON));

        $result = $this->docs->quoteProject(new Project(json_decode(self::$PROJECT_JSON)));

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_QUOTED_STATUS));
    }

    /**
     * Test a call to quote a project when no id is provided.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testQuotedProjectNoId()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $project->setId(null);

        $this->docs->quoteProject($project);
    }

    /**
     * Test a call to delete a project.
     */
    public function testDeleteProject()
    {
        $this->docs
            ->shouldReceive('delete')
            ->with(Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID);

        $this->docs->deleteProject(new Project(json_decode(self::$PROJECT_JSON)));
    }

    /**
     * Test a call to delete a project which has been quoted.
     */
    public function testDeleteQuotedProject()
    {
        $this->docs
            ->shouldReceive('delete')
            ->with(Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID);

        $this->docs->deleteProject(new Project(json_decode(self::$QUOTED_PROJECT_JSON)));
    }

    /**
     * Test the calculate project price method for a project with an analysed job.
     */
    public function testCalculateProjectPrice()
    {
        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID . '/' . Docs::$PRICE_ENDPOINT,
                array(),
                false
            )
            ->andReturn(self::$PROJECT_PRICE_JSON);

        $project = new Project(json_decode(self::$PROJECT_JSON));

        $projectPrice = $this->docs->calculateProjectPrice($project);

        $this->assertNotNull($projectPrice);
        $this->assertEquals($projectPrice['price'], self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($projectPrice['currency'], self::$CURRENCY);
    }

    /**
     * Test a call to get a project's price.
     */
    public function testGetProjectPrice()
    {
        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID . '/' . Docs::$PRICE_ENDPOINT,
                array(),
                false
            )
            ->andReturn(self::$PROJECT_PRICE_JSON);

        $project = new Project(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->getProjectPrice($project);

        $this->assertNotNull($result);
        $this->assertEquals($result->getCurrency(), self::$CURRENCY);
        $this->assertEquals($result->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);
    }

    /**
     * Test a call to get a project's price using the price href.
     */
    public function testGetProjectPriceByHref()
    {
        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID . '/' . Docs::$PRICE_ENDPOINT,
                array(),
                false
            )
            ->andReturn(self::$PROJECT_PRICE_JSON);

        $project = new Project(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->getProjectPrice($project->getLink(Project::PRICE_REL));

        $this->assertNotNull($result);
        $this->assertEquals($result->getCurrency(), self::$CURRENCY);
        $this->assertEquals($result->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);
    }

    /**
     * Test a call to get a price for unknown project.
     */
    public function testGetPriceForUnknownProject()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException(''));

        $result = $this->docs->getProjectPrice(self::$PROJECT_PRICE_JSON);

        $this->assertNull($result);
    }

    /**
     * Test a call to get project charges by project.
     */
    public function testGetProjectChargesByProject()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$PROJECT_CHARGES_JSON);

        $project = new Project(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->getProjectCharges($project, 5);

        $links = $result[0]->getLinks();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getTitle(), self::$CHARGE_TITLE);
        $this->assertEquals($result[0]->getValue(), self::$CHARGE_VALUE);
    }

    /**
     * Test a call to get project charges by project id.
     */
    public function testGetProjectChargesByProjectId()
    {
        $this->docs
            ->shouldReceive('get')
            ->with($this->docs->getBaseUrl() . Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID, false, array())
            ->andReturn(self::$PROJECT_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID . '/' . Docs::$PROJECT_CHARGES_ENDPOINT,
                array('size' => 5, 'page' => 0),
                false
            )
            ->andReturn(self::$PROJECT_CHARGES_JSON);

        $result = $this->docs->getProjectCharges(self::$PROJECT_ID, 5);

        $links = $result[0]->getLinks();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getTitle(), self::$CHARGE_TITLE);
        $this->assertEquals($result[0]->getValue(), self::$CHARGE_VALUE);
    }

    /**
     * Test a call to delete an in progress project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testDeleteInProgressProject()
    {
        $this->docs->deleteProject(new Project(json_decode(self::$IN_PROGRESS_PROJECT_JSON)));
    }

    public function testDeleteJob()
    {
        $this->docs
            ->shouldReceive('delete')
            ->with(Docs::$JOB_ENDPOINT . '/' . self::$JOB_ID);

        $this->docs->deleteJob(new Job(json_decode(self::$JOB_JSON)));
    }

    /**
     * Test a call to delete an in progress project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testDeleteInProgressJob()
    {
        $this->docs->deleteJob(new Job(json_decode(self::$IN_PROGRESS_JOB_JSON)));
    }

    /**
     * Test a call to confirm a project.
     */
    public function testConfirmProject()
    {
        $this->docs = \Mockery::mock(
            'Lingo24\API\Docs[updateProject]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->docs
            ->shouldReceive('updateProject')
            ->andReturn(new Project(json_decode(self::$PROJECT_JSON)));

        $result = $this->docs->confirmProject(new Project(json_decode(self::$PROJECT_JSON)));

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to confirm an unknown project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testConfirmUnkownProject()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $project->setId(null);

        $this->docs->confirmProject($project);
    }

    /**
     * Test a call to confirm an unknown project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testConfirmConfirmedProject()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $project->setStatus(ProjectStatus::IN_PROGRESS);

        $this->docs->confirmProject($project);
    }

    /**
     * Test a call to cancel a project.
     */
    public function testCancelProject()
    {
        $this->docs = \Mockery::mock(
            'Lingo24\API\Docs[updateProject]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->docs
            ->shouldReceive('updateProject')
            ->andReturn(new Project(json_decode(self::$PROJECT_JSON)));

        $result = $this->docs->cancelProject(new Project(json_decode(self::$PROJECT_JSON)));

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$PROJECT_ID);
        $this->assertEquals($result->getName(), self::$PROJECT_NAME);
        $this->assertEquals($result->getCreated(), (new \DateTime())->setTimestamp(self::$PROJECT_CREATED));
        $this->assertEquals($result->getStatus(), new ProjectStatus(self::$PROJECT_STATUS));
    }

    /**
     * Test a call to cancel an unknown project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCancelUnkownProject()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $project->setId(null);

        $this->docs->cancelProject($project);
    }

    /**
     * Test the getBaseUrl() method when no environment is specified.
     */
    public function testGetBaseUrl()
    {
        $this->docs = new Docs(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI);

        $this->assertEquals($this->docs->getBaseUrl(), Docs::$BASE_URL_LIVE);
    }

    /**
     * Test the getBaseUrl() method when the demo environment is specified.
     */
    public function testGetBaseUrlDemo()
    {
        $this->docs = new Docs(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI, null, Docs::$DEMO_ENV);

        $this->assertEquals($this->docs->getBaseUrl(), Docs::$BASE_URL_DEMO);
    }

    /**
     * Test a call to get a file.
     */
    public function testGetFile()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$FILE_JSON);

        $result = $this->docs->getFile(self::$FILE_ID);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$FILE_ID);
        $this->assertEquals($result->getName(), self::$FILE_NAME);
        $this->assertEquals($result->getType(), self::$FILE_TYPE);
    }

    /**
     * Test a call to get a file using the job.
     */
    public function testGetFileByJob()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$FILE_JSON);

        $result = $this->docs->getFile($job, FileType::SOURCE);

        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$FILE_ID);
        $this->assertEquals($result->getName(), self::$FILE_NAME);
        $this->assertEquals($result->getType(), self::$FILE_TYPE);
    }

    /**
     * Test a call to get a file using the job.
     */
    public function testGetFileByJobNoStatus()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $this->docs
        ->shouldReceive('get')
        ->andReturn(self::$FILE_JSON);

        $result = $this->docs->getFile($job);

        $this->assertNull($result);
    }

    /**
     * Test a call to get a unknown file.
     */
    public function testGetUnknownFile()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException(''));

        $result = $this->docs->getFile(self::$UNKNOWN_FILE_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to create a file.
     */
    public function testCreateFile()
    {
        $this->docs
            ->shouldReceive('postJson')
            ->andReturn(json_decode(self::$FILE_JSON));

        $this->docs
            ->shouldReceive('put')
            ->andReturn('');

        $temp_file = tempnam(sys_get_temp_dir(), 'Test');
        $result = $this->docs->createFile(new File(json_decode(self::$FILE_JSON)), $temp_file);

        $this->assertNotNull($result);
        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$FILE_ID);
        $this->assertEquals($result->getName(), self::$FILE_NAME);
        $this->assertEquals($result->getType(), self::$FILE_TYPE);
    }

    /**
     * Test a call to create a file by name.
     */
    public function testCreateFileByName()
    {
        $this->docs
            ->shouldReceive('postJson')
            ->with($this->docs->getBaseUrl() . Docs::$FILE_ENDPOINT, array('name'=>'test.txt','type'=>'SOURCE'))
            ->andReturn(json_decode(self::$FILE_JSON));

        $this->docs
            ->shouldReceive('put')
            ->andReturn('');

        $temp_file = tempnam(sys_get_temp_dir(), 'Test');
        $result = $this->docs->createFile(self::$FILE_NAME, $temp_file);

        $this->assertNotNull($result);
        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$FILE_ID);
        $this->assertEquals($result->getName(), self::$FILE_NAME);
        $this->assertEquals($result->getType(), self::$FILE_TYPE);
    }

    /**
     * Test a call to delete a file.
     */
    public function testDeleteFile()
    {
        $this->docs
            ->shouldReceive('delete')
            ->with(Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID);

        $this->docs->deleteFile(new File(json_decode(self::$FILE_JSON)));
    }

    /**
     * Test a call to get a file's content.
     */
    public function testGetFileContent()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$FILE_CONTENT);

        $result = $this->docs->getFileContent(self::$FILE_ID);

        $this->assertNotNull($result);
        $this->assertEquals($result, self::$FILE_CONTENT);
    }

    /**
     * Test a call to get a file's content by file.
     */
    public function testGetFileContentByFile()
    {
        $file = new File(json_decode(self::$FILE_JSON));

        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$FILE_CONTENT);

        $result = $this->docs->getFileContent($file);

        $this->assertNotNull($result);
        $this->assertEquals($result, self::$FILE_CONTENT);
    }

    /**
     * Test a call to get a file's content by job.
     */
    public function testGetFileContentByJob()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID, array(), false)
            ->andReturn(self::$FILE_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID . '/' . File::CONTENT_REL)
            ->andReturn(self::$FILE_CONTENT);

        $result = $this->docs->getFileContent($job, FileType::SOURCE);

        $this->assertNotNull($result);
        $this->assertEquals($result, self::$FILE_CONTENT);
    }

    /**
     * Test a call to get a file's content by job with a missing file.
     */
    public function testGetFileContentByJobMissingFile()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID, array(), false)
            ->andThrow(new Lingo24ApiNotFoundException());

        $result = $this->docs->getFileContent($job, FileType::SOURCE);

        $this->assertNull($result);
    }

    /**
     * Test a call to get a file's content by job with no file.
     */
    public function testGetFileContentByJobNoFile()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $result = $this->docs->getFileContent($job, FileType::TARGET);

        $this->assertNull($result);
    }

    /**
     * Test a call to get a file's content by href.
     */
    public function testGetFileContentByHref()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$FILE_CONTENT);

        $result = $this->docs->getFileContent(
            Docs::$BASE_URL_LIVE . Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID . '/' . Docs::$FILE_CONTENT_ENDPOINT
        );

        $this->assertNotNull($result);
        $this->assertEquals($result, self::$FILE_CONTENT);
    }

    /**
     * Test a call to get a file's content, when the content can't be found.
     */
    public function testGetFileContentNotFound()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException());

        $result = $this->docs->getFileContent(self::$FILE_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to get a file's content, when there is an error.
     */
    public function testGetFileContentError()
    {
        $this->docs
        ->shouldReceive('get')
        ->andThrow(new Lingo24ApiException());

        $result = $this->docs->getFileContent(self::$FILE_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to get jobs by project.
     */
    public function testGetJobsByProject()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$JOBS_JSON);

        $project = new Project(json_decode(self::$PROJECT_JSON));

        $result = $this->docs->getJobs($project, 5);

        $links = $result[0]->getLinks();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getId(), self::$JOB_ID);
        $this->assertEquals(self::$JOB_STATUS_STRING, $result[0]->getStatus());

        $this->assertEquals(
            $links['service'],
            Docs::$SERVICE_ENDPOINT . '/' . self::$JOB_SERVICE_ID
        );
        $this->assertEquals(
            $links['source-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_SOURCE_LOCALE_ID
        );
        $this->assertEquals(
            $links['target-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_TARGET_LOCALE_ID
        );
        $this->assertEquals(
            $links['source-file'],
            Docs::$FILE_ENDPOINT . '/' . self::$JOB_SOURCE_FILE_ID
        );
        $this->assertEquals(
            $links['project'],
            Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID
        );
    }


    /**
     * Test a call to get jobs by project id.
     */
    public function testGetJobsByProjectId()
    {
        $this->docs
            ->shouldReceive('get')
            ->with($this->docs->getBaseUrl() . Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID, false, array())
            ->andReturn(self::$PROJECT_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID . '/' . Docs::$JOB_ENDPOINT,
                array('size' => 5, 'page' => 0),
                false
            )
            ->andReturn(self::$JOBS_JSON);

        $result = $this->docs->getJobs(self::$PROJECT_ID, 5);

        $links = $result[0]->getLinks();

        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getId(), self::$JOB_ID);
        $this->assertEquals(self::$JOB_STATUS_STRING, $result[0]->getStatus());

        $this->assertEquals(
            $links['service'],
            Docs::$SERVICE_ENDPOINT . '/' . self::$JOB_SERVICE_ID
        );
        $this->assertEquals(
            $links['source-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_SOURCE_LOCALE_ID
        );
        $this->assertEquals(
            $links['target-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_TARGET_LOCALE_ID
        );
        $this->assertEquals(
            $links['source-file'],
            Docs::$FILE_ENDPOINT . '/' . self::$JOB_SOURCE_FILE_ID
        );
        $this->assertEquals(
            $links['project'],
            Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID
        );
    }

    /**
     * Test a call to get job a job by id.
     */
    public function testGetJob()
    {
        $this->docs
            ->shouldReceive('get')
            ->with($this->docs->getBaseUrl() . Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID, array(), false)
            ->andReturn(self::$PROJECT_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID, array(), false)
            ->andReturn(self::$PROJECT_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$SERVICE_ENDPOINT . '/' . self::$SERVICE_ID, array(), false)
            ->andReturn(self::$SERVICE_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$LOCALE_ENDPOINT . '/' . self::$LOCALE_ID_1, array(), false)
            ->andReturn(self::$LOCALE_JSON_1);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$LOCALE_ENDPOINT . '/' . self::$LOCALE_ID_2, array(), false)
            ->andReturn(self::$LOCALE_JSON_2);

        $this->docs
            ->shouldReceive('get')
            ->with(Docs::$FILE_ENDPOINT . '/' . self::$FILE_ID, array(), false)
            ->andReturn(self::$FILE_JSON);

        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID.  '/' . Docs::$JOB_ENDPOINT . '/' . self::$JOB_ID,
                array(),
                false
            )
            ->andReturn(self::$JOB_JSON);

        $result = $this->docs->getJob(self::$PROJECT_ID, self::$JOB_ID);
        $links = $result->getLinks();

        $this->assertEquals($result->getId(), self::$JOB_ID);
        $this->assertEquals(self::$JOB_STATUS_STRING, $result->getStatus());

        $this->assertEquals(
            $links['service'],
            Docs::$SERVICE_ENDPOINT . '/' . self::$JOB_SERVICE_ID
        );
        $this->assertEquals(
            $links['source-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_SOURCE_LOCALE_ID
        );
        $this->assertEquals(
            $links['target-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_TARGET_LOCALE_ID
        );
        $this->assertEquals(
            $links['source-file'],
            Docs::$FILE_ENDPOINT . '/' . self::$JOB_SOURCE_FILE_ID
        );
        $this->assertEquals(
            $links['project'],
            Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID
        );
    }

    /**
     * Test a call to get a job's price.
     */
    public function testGetJobPrice()
    {
        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$JOB_ENDPOINT . '/' . self::$JOB_ID . '/' . Docs::$PRICE_ENDPOINT,
                array(),
                false
            )
            ->andReturn(self::$JOB_PRICE_JSON);

        $job = new Job(json_decode(self::$JOB_JSON));

        $result = $this->docs->getJobPrice($job);

        $this->assertNotNull($result);
        $this->assertEquals($result->getCurrency(), self::$CURRENCY);
        $this->assertEquals($result->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);
    }

    /**
     * Test a call to get a job's price using the price href.
     */
    public function testGetJobPriceByHref()
    {
        $this->docs
            ->shouldReceive('get')
            ->with(
                Docs::$JOB_ENDPOINT . '/' . self::$JOB_ID . '/' . Docs::$PRICE_ENDPOINT,
                array(),
                false
            )
            ->andReturn(self::$JOB_PRICE_JSON);

        $job = new Job(json_decode(self::$JOB_JSON));

        $result = $this->docs->getJobPrice($job->getLink(Job::PRICE_REL));

        $this->assertNotNull($result);
        $this->assertEquals($result->getCurrency(), self::$CURRENCY);
        $this->assertEquals($result->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($result->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);
    }

    /**
     * Test a call to get a price for unknown project.
     */
    public function testGetPriceForUnknownJob()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException(''));

        $result = $this->docs->getJobPrice(self::$JOB_PRICE_JSON);

        $this->assertNull($result);
    }

    /**
     * Test a call to get a unknown project.
     */
    public function testGetUnknownJob()
    {
        $this->docs
            ->shouldReceive('get')
            ->with($this->docs->getBaseUrl() . Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID, false, array())
            ->andReturn(self::$PROJECT_JSON);

        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException(''));

        $result = $this->docs->getJob(self::$PROJECT_ID, self::$JOB_ID);

        $this->assertNull($result);
    }

    /**
     * Test a call to create a job.
     */
    public function testCreateJob()
    {
        $this->docs
            ->shouldReceive('postJson')
            ->andReturn(json_decode(self::$JOB_JSON));

        $job = $this->createJobModel();
        $result = $this->docs->createJob($job);

        $links = $result->getLinks();

        $this->assertEquals($result->getId(), self::$JOB_ID);
        $this->assertEquals(self::$JOB_STATUS_STRING, $result->getStatus());

        $this->assertEquals(
            $links['service'],
            Docs::$SERVICE_ENDPOINT . '/' . self::$JOB_SERVICE_ID
        );
        $this->assertEquals(
            $links['source-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_SOURCE_LOCALE_ID
        );
        $this->assertEquals(
            $links['target-locale'],
            Docs::$LOCALE_ENDPOINT . '/' . self::$JOB_TARGET_LOCALE_ID
        );
        $this->assertEquals(
            $links['source-file'],
            Docs::$FILE_ENDPOINT . '/' . self::$JOB_SOURCE_FILE_ID
        );
        $this->assertEquals(
            $links['project'],
            Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID
        );
    }

    /**
     * Test a call to create a job without project.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCreateJobNoId()
    {
        $job = $this->createJobModel();
        $job->setProject(null);
        $job->addLink(Job::PROJECT_REL, null);

        $this->docs->createJob($job);
    }

    /**
     * Test a call to create a job without service.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCreateJobNoService()
    {
        $job = $this->createJobModel();
        $job->setService(null);
        $job->addLink(Job::SERVICE_REL, null);

        $this->docs->createJob($job);
    }

    /**
     * Test a call to create a job without source locale.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCreateJobNoSourceLocale()
    {
        $job = $this->createJobModel();
        $job->setSourceLocale(null);
        $job->addLink(Job::SOURCE_LOCALE_REL, null);

        $this->docs->createJob($job);
    }

    /**
     * Test a call to create a job without target locale.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCreateJobNoTargetLocale()
    {
        $job = $this->createJobModel();
        $job->setTargetLocale(null);
        $job->addLink(Job::TARGET_LOCALE_REL, null);

        $this->docs->createJob($job);
    }

    /**
     * Test a call to create a job without file.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testCreateJobNoFourceFile()
    {
        $job = $this->createJobModel();
        $job->setSourceFile(null);
        $job->addLink(Job::SOURCE_FILE_REL, null);

        $this->docs->createJob($job);
    }

    /**
     * Test a call to update a job.
     */
    public function testUpdateJob()
    {
        $this->docs
            ->shouldReceive('putJson')
            ->andReturn(json_decode(self::$JOB_JSON));

        $job = $this->createJobModel();

        $result = $this->docs->updateJob($job);
        $this->assertNotNull($result);
        $this->assertEquals($result->getId(), self::$JOB_ID);
        $this->assertEquals(self::$JOB_STATUS_STRING, $result->getStatus());
    }

    /**
     * Test a call to update a job without id.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateJobNoId()
    {
        $job = $this->createJobModel();
        $job->setId(null);

        $this->docs->updateJob($job);
    }

    /**
     * Test a call to update a job without service.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateJobNoService()
    {
        $job = $this->createJobModel();
        $job->setService(null);

        $this->docs->updateJob($job);
    }

    /**
     * Test a call to update a job without source locale.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateJobNoSourceLocale()
    {
        $job = $this->createJobModel();
        $job->setSourceLocale(null);

        $this->docs->updateJob($job);
    }

    /**
     * Test a call to update a job without target locale.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateJobNoTargetLocale()
    {
        $job = $this->createJobModel();
        $job->setTargetLocale(null);

        $this->docs->updateJob($job);
    }

    /**
     * Test a call to update a job without file.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testUpdateJobNoSourceFile()
    {
        $job = $this->createJobModel();
        $job->setSourceFile(null);

        $this->docs->updateJob($job);
    }

    /**
     * Test a call to get job metrics.
     */
    public function testGetJobMetrics()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$JOB_METRICS_JSON);

        $job = $this->createJobModel();
        $result = $this->docs->getJobMetrics($job);

        $this->assertEquals(count($result), 13);
        $this->assertEquals($result['FUZZY_MATCH_75_84']->getWords(), 0);
        $this->assertEquals($result['REPETITION_75_84']->getWords(), 5);
        $this->assertEquals($result['REPETITION_100']->getWords(), 0);
        $this->assertEquals($result['REPETITION_95_99']->getWords(), 0);
        $this->assertEquals($result['IN_CONTEXT_EXACT_MATCH']->getWords(), 0);
        $this->assertEquals($result['TOTAL']->getWords(), 2);
        $this->assertEquals($result['NON_TRANSLATABLE']->getWords(), 0);
        $this->assertEquals($result['LEVERAGED_MATCH']->getWords(), 0);
        $this->assertEquals($result['FUZZY_MATCH_85_94']->getWords(), 0);
        $this->assertEquals($result['REPETITION_ICE']->getWords(), 0);
        $this->assertEquals($result['REPETITION_85_94']->getWords(), 0);
        $this->assertEquals($result['FUZZY_MATCH_95_99']->getWords(), 0);
        $this->assertEquals($result['NO_MATCH']->getWords(), 2);
    }

    /**
     * Test a call to get job metrics with a 404 error.
     */
    public function testGetJobMetricsUnknown()
    {
        $this->docs
            ->shouldReceive('get')
            ->andThrow(new Lingo24ApiNotFoundException());

        $job = $this->createJobModel();
        $result = $this->docs->getJobMetrics($job);

        $this->assertNull($result);
    }

    /**
     * @return Job
     */
    private function createJobModel($project = null)
    {
        if (is_null($project)) {
            $project = new Project(json_decode(self::$PROJECT_JSON));
        }

        $job = new Job(json_decode(self::$JOB_JSON), $project);
        $job->setService(new Service(json_decode(self::$SERVICES_JSON)->content[0]));
        $job->setSourceLocale(new TranslationLocale(json_decode(self::$LOCALES_JSON)->content[0]));
        $job->setTargetLocale(new TranslationLocale(json_decode(self::$LOCALES_JSON)->content[1]));
        $job->setSourceFile(new File(json_decode(self::$FILE_JSON)));

        return $job;
    }

    /**
     * Test the getAuthUrl() method when no environment is specified.
     */
    public function testGetAuthUrl()
    {
        $this->docs = new Docs(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI);

        $this->assertEquals($this->docs->getAuthUrl(), Docs::$AUTH_URL_LIVE);
    }

    /**
     * Test the getAuthUrl() method when the demo environment is specified.
     */
    public function testGetAuthUrlDemo()
    {
        $this->docs = new Docs(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI, null, Docs::$DEMO_ENV);

        $this->assertEquals($this->docs->getAuthUrl(), Docs::$AUTH_URL_DEMO);
    }

    /**
     * Test a call to get tags, firstly where they need to be fetched and then again to check they are cached.
     */
    public function testGetTags()
    {
        $this->docs
            ->shouldReceive('get')
            ->andReturn(self::$TAGS_JSON);

        $result = $this->docs->getTags();

        $this->assertEquals(count($result), 2);
        $this->assertEquals($result[0], self::$TAG);

        $cache = $this->docs->getTags();

        $this->assertEquals($cache, $result);
    }
}
