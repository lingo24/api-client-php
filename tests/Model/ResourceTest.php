<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;

class ResourceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $REL_1  = 'rel1';
    private static $HREF_1 = 'href1';
    private static $REL_2  = 'rel2';
    private static $HREF_2 = 'href2';

    /**
     * Test adding and retrieving links.
     */
    public function testLinks()
    {
        $resource = new TestResource(json_decode('{"links": [{"rel": "rel1", "href": "href1"}]}'));

        $resource->addLink(self::$REL_2, self::$HREF_2);

        $this->assertEquals(self::$HREF_1, $resource->getLink(self::$REL_1));
        $this->assertEquals(self::$HREF_2, $resource->getLink(self::$REL_2));

        $links = $resource->getLinks();
        $this->assertEquals($links[self::$REL_1], self::$HREF_1);
        $this->assertEquals($links[self::$REL_2], self::$HREF_2);
    }

    /**
     * Test the self relationship special case.
     */
    public function testSelf()
    {
        $resource = new TestResource(null);

        $resource->addLink(Resource::SELF_REL, self::$HREF_1);

        $this->assertEquals($resource->getSelf(), self::$HREF_1);
    }
}
