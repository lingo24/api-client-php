<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\FileType;

class FileTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $SOURCE = 'SOURCE';
    private static $TARGET = 'TARGET';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorToStringEquals()
    {
        $sourceFileType = new FileType(self::$SOURCE);
        $targetFileType = new FileType(self::$TARGET);

        $this->assertTrue($sourceFileType->equals(new FileType(self::$SOURCE)));
        $this->assertTrue($sourceFileType->equals(FileType::SOURCE));
        $this->assertEquals((string) $sourceFileType, self::$SOURCE);

        $this->assertTrue($targetFileType->equals(new FileType(self::$TARGET)));
        $this->assertTrue($targetFileType->equals(FileType::TARGET));
        $this->assertEquals((string) $targetFileType, self::$TARGET);

        $this->assertFalse($sourceFileType->equals($targetFileType));
    }
}
