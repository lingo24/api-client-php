<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\TranslationLocale;

class TranslationLocaleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1        = 1;
    private static $LANGUAGE_1  = "language1";
    private static $COUNTRY_1   = 'country1';
    private static $ID_2        = 2;
    private static $LANGUAGE_2  = "language2";
    private static $COUNTRY_2   = 'country2';
    private static $LOCALE_JSON =
        '{"id": 1, "language": "language1", "country": "country1", "links": [{"rel": "self", "href": "locales/1"}]}';
    private static $CMP1_JSON   = '{"id": 1, "language": "a", "country": "a"}';
    private static $CMP2_JSON   = '{"id": 2, "language": "a", "country": "a"}';
    private static $CMP3_JSON   = '{"id": 1, "language": "a", "country": "b"}';
    private static $CMP4_JSON   = '{"id": 2, "language": "b", "country": "a"}';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $locale = new TranslationLocale(json_decode(self::$LOCALE_JSON));

        $this->assertEquals($locale->getId(), self::$ID_1);
        $this->assertEquals($locale->getLanguage(), self::$LANGUAGE_1);
        $this->assertEquals($locale->getCountry(), self::$COUNTRY_1);
        $this->assertEquals($locale->getSelf(), Docs::$LOCALE_ENDPOINT . "/" . self::$ID_1);

        $locale->setId(self::$ID_2);
        $locale->setLanguage(self::$LANGUAGE_2);
        $locale->setCountry(self::$COUNTRY_2);

        $this->assertEquals($locale->getId(), self::$ID_2);
        $this->assertEquals($locale->getLanguage(), self::$LANGUAGE_2);
        $this->assertEquals($locale->getCountry(), self::$COUNTRY_2);
    }

    /**
     * Test the model's toString() returns the language and country.
     */
    public function testToString()
    {
        $locale = new TranslationLocale(json_decode(self::$LOCALE_JSON));
        $this->assertEquals((string) $locale, self::$LANGUAGE_1 . '_' . self::$COUNTRY_1);
    }

    /**
     * Test the locale comparisons, based on the language_country format returned by the toString().
     */
    public function testCmp()
    {
        $locale1 = new TranslationLocale(json_decode(self::$CMP1_JSON));
        $locale2 = new TranslationLocale(json_decode(self::$CMP2_JSON));
        $locale3 = new TranslationLocale(json_decode(self::$CMP3_JSON));
        $locale4 = new TranslationLocale(json_decode(self::$CMP4_JSON));

        $this->assertEquals(0, TranslationLocale::cmp($locale1, $locale2));
        $this->assertLessThan(0, TranslationLocale::cmp($locale1, $locale3));
        $this->assertLessThan(0, TranslationLocale::cmp($locale1, $locale4));
        $this->assertLessThan(0, TranslationLocale::cmp($locale2, $locale3));
    }
}
