<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\JobMetric;

class JobMetricTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $CHARACTERS_1    = 1;
    private static $SEGMENTS_1      = 2;
    private static $WORDS_1         = 3;
    private static $WHITE_SPACES_1  = 4;
    private static $CHARACTERS_2    = 5;
    private static $SEGMENTS_2      = 6;
    private static $WORDS_2         = 7;
    private static $WHITE_SPACES_2  = 8;
    private static $JOB_METRIC_JSON = '{"CHARACTERS": 1, "SEGMENTS": 2, "WORDS": 3, "WHITE_SPACES": 4}';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $jobMetric = new JobMetric(json_decode(self::$JOB_METRIC_JSON));

        $this->assertEquals($jobMetric->getCharacters(), self::$CHARACTERS_1);
        $this->assertEquals($jobMetric->getSegments(), self::$SEGMENTS_1);
        $this->assertEquals($jobMetric->getWords(), self::$WORDS_1);
        $this->assertEquals($jobMetric->getWhiteSpaces(), self::$WHITE_SPACES_1);

        $jobMetric->setCharacters(self::$CHARACTERS_2);
        $jobMetric->setSegments(self::$SEGMENTS_2);
        $jobMetric->setWords(self::$WORDS_2);
        $jobMetric->setWhiteSpaces(self::$WHITE_SPACES_2);

        $this->assertEquals($jobMetric->getCharacters(), self::$CHARACTERS_2);
        $this->assertEquals($jobMetric->getSegments(), self::$SEGMENTS_2);
        $this->assertEquals($jobMetric->getWords(), self::$WORDS_2);
        $this->assertEquals($jobMetric->getWhiteSpaces(), self::$WHITE_SPACES_2);
    }
}
