<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\File;
use Lingo24\API\Model\FileType;

class FileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1      = 1;
    private static $NAME_1    = "name1";
    private static $TYPE_1    = 'SOURCE';
    private static $ID_2      = 2;
    private static $NAME_2    = "name2";
    private static $TYPE_2    = 'TARGET';
    private static $FILE_JSON = <<<END
{
    "id": 1,
    "name": "name1",
    "type": "SOURCE",
    "links": [{"rel": "self", "href": "files/1"}, {"rel": "content", "href": "files/1/content"}]
}
END;

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $file = new File(json_decode(self::$FILE_JSON));

        $this->assertEquals($file->getId(), self::$ID_1);
        $this->assertEquals($file->getName(), self::$NAME_1);
        $this->assertEquals($file->getType(), new FileType(self::$TYPE_1));
        $this->assertEquals($file->getSelf(), Docs::$FILE_ENDPOINT . "/" . self::$ID_1);
        $this->assertEquals(
            $file->getLink(File::CONTENT_REL),
            Docs::$FILE_ENDPOINT . "/" . self::$ID_1 . '/' . Docs::$FILE_CONTENT_ENDPOINT
        );

        $file->setId(self::$ID_2);
        $file->setName(self::$NAME_2);
        $file->setType(new FileType(self::$TYPE_2));

        $this->assertEquals($file->getId(), self::$ID_2);
        $this->assertEquals($file->getName(), self::$NAME_2);
        $this->assertEquals($file->getType(), new FileType(self::$TYPE_2));

        $file->setType(self::$TYPE_1);
        $this->assertEquals($file->getType(), new FileType(self::$TYPE_1));
    }

    /**
     * Test the model's toString() returns the file name.
     */
    public function testToString()
    {
        $file = new File(json_decode(self::$FILE_JSON));
        $this->assertEquals((string) $file, self::$NAME_1);
    }
}
