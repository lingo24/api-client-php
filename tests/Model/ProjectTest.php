<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\Project;
use Lingo24\API\Model\ProjectStatus;

class ProjectTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1         = 1;
    private static $NAME_1       = "name1";
    private static $CREATED_1    = 1;
    private static $STATUS_1     = 'CREATED';
    private static $ID_2         = 2;
    private static $NAME_2       = "name2";
    private static $CREATED_2    = 2;
    private static $STATUS_2     = 'QUOTED';
    private static $STATUS_3     = 'IN_PROGRESS';
    private static $TAG_1        = 'Tag One';
    private static $TAGS_1       = array('Tag One');
    private static $TAGS_2       = array('Tag Two');
    private static $TAGS_3       = array('Tag Two', 'Tag One');
    private static $PROJECT_JSON = <<<END
{
  "id": 1,
  "name": "name1",
  "created": 1,
  "projectStatus": "CREATED",
  "tags": ["Tag One"],
  "links": [{"rel": "self", "href": "projects/1"}]
}
END;
    private static $NEW_PROJECT_JSON = '{"name": "name1", "created": 1, "projectStatus": "CREATED"}';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $this->assertEquals($project->getId(), self::$ID_1);
        $this->assertEquals($project->getName(), self::$NAME_1);
        $this->assertEquals($project->getCreated(), (new \DateTime())->setTimestamp(self::$CREATED_1));
        $this->assertEquals($project->getStatus(), new ProjectStatus(self::$STATUS_1));
        $this->assertEquals($project->getTags(), self::$TAGS_1);
        $this->assertEquals($project->getSelf(), Docs::$PROJECT_ENDPOINT . "/" . self::$ID_1);


        $project->setId(self::$ID_2);
        $project->setName(self::$NAME_2);
        $project->setCreated((new \DateTime())->setTimestamp(self::$CREATED_2));
        $project->setStatus(new ProjectStatus(self::$STATUS_2));
        $project->setTags(self::$TAGS_2);

        $this->assertEquals($project->getId(), self::$ID_2);
        $this->assertEquals($project->getName(), self::$NAME_2);
        $this->assertEquals($project->getCreated(), (new \DateTime())->setTimestamp(self::$CREATED_2));
        $this->assertEquals($project->getStatus(), new ProjectStatus(self::$STATUS_2));
        $this->assertEquals($project->getTags(), self::$TAGS_2);

        $project->setCreated(self::$CREATED_1);
        $project->setStatus(self::$STATUS_3);
        $project->addTag(self::$TAG_1);

        $this->assertEquals($project->getCreated(), (new \DateTime())->setTimestamp(self::$CREATED_1));
        $this->assertEquals($project->getStatus(), new ProjectStatus(self::$STATUS_3));
        $this->assertEquals($project->getTags(), self::$TAGS_3);

        $project->setStatus(ProjectStatus::FINISHED);
        $project->setTags(self::$TAG_1);

        $this->assertEquals($project->getStatus(), new ProjectStatus(ProjectStatus::FINISHED));
        $this->assertEquals($project->getTags(), self::$TAGS_3);
    }

    public function testConstructorNoId()
    {
        $project = new Project(json_decode(self::$NEW_PROJECT_JSON));

        $this->assertEquals($project->getName(), self::$NAME_1);
        $this->assertEquals($project->getCreated(), (new \DateTime())->setTimestamp(self::$CREATED_1));
        $this->assertEquals($project->getStatus(), new ProjectStatus(self::$STATUS_1));
    }

    /**
     * Test the model's toString() returns the project name.
     */
    public function testToString()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));
        $this->assertEquals((string) $project, self::$NAME_1);
    }
}
