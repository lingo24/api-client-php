<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\Service;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1          = 1;
    private static $NAME_1        = "name1";
    private static $DESCRIPTION_1 = 'description1';
    private static $ID_2          = 2;
    private static $NAME_2        = "name2";
    private static $DESCRIPTION_2 = 'description2';
    private static $SERVICE_JSON  =
        '{"id": 1, "name": "name1", "description": "description1", "links": [{"rel": "self", "href": "services/1"}]}';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $service = new Service(json_decode(self::$SERVICE_JSON));

        $this->assertEquals($service->getId(), self::$ID_1);
        $this->assertEquals($service->getName(), self::$NAME_1);
        $this->assertEquals($service->getDescription(), self::$DESCRIPTION_1);
        $this->assertEquals($service->getSelf(), Docs::$SERVICE_ENDPOINT . "/" . self::$ID_1);

        $service->setId(self::$ID_2);
        $service->setName(self::$NAME_2);
        $service->setDescription(self::$DESCRIPTION_2);

        $this->assertEquals($service->getId(), self::$ID_2);
        $this->assertEquals($service->getName(), self::$NAME_2);
        $this->assertEquals($service->getDescription(), self::$DESCRIPTION_2);
    }

    /**
     * Test the model's toString() returns the service name.
     */
    public function testToString()
    {
        $service = new Service(json_decode(self::$SERVICE_JSON));
        $this->assertEquals((string) $service, self::$NAME_1);
    }
}
