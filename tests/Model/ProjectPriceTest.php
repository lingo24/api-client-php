<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\ProjectPrice;

class ProjectPriceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $CURRENCY                 = 'GBP';
    private static $TOTAL_WO_VAT_W_DISCOUNT  = 0.29;
    private static $TOTAL_WO_VAT_WO_DISCOUNT = 0.29;
    private static $TOTAL_W_VAT_W_DISCOUNT   = 0.35;
    private static $TOTAL_W_VAT_WO_DISCOUNT  = 0.35;
    private static $PRICE_JSON               = <<<END
{
    "currencyCode": "GBP",
    "totalWoVatWDiscount": 0.29,
    "totalWoVatWoDiscount": 0.29,
    "totalWVatWDiscount": 0.35,
    "totalWVatWoDiscount": 0.35
}
END;

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $projectPrice = new ProjectPrice(json_decode(self::$PRICE_JSON));

        $this->assertEquals($projectPrice->getCurrency(), self::$CURRENCY);
        $this->assertEquals($projectPrice->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($projectPrice->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($projectPrice->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($projectPrice->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);

        $projectPrice->setCurrency(self::$CURRENCY);
        $projectPrice->setTotalWoVatWDiscount(self::$TOTAL_WO_VAT_W_DISCOUNT);
        $projectPrice->setTotalWoVatWoDiscount(self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $projectPrice->setTotalWVatWDiscount(self::$TOTAL_W_VAT_W_DISCOUNT);
        $projectPrice->setTotalWVatWoDiscount(self::$TOTAL_W_VAT_WO_DISCOUNT);
    }
}
