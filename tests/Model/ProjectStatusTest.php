<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\ProjectStatus;

class ProjectStatusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $CREATED     = 'CREATED';
    private static $PENDING     = 'PENDING';
    private static $QUOTED      = 'QUOTED';
    private static $IN_PROGRESS = 'IN_PROGRESS';
    private static $CANCELLED   = 'CANCELLED';
    private static $FINISHED    = 'FINISHED';
    private static $UNKNOWN     = 0;

    private static $CREATED_STRING     = 'Created';
    private static $PENDING_STRING     = 'Pending';
    private static $QUOTED_STRING      = 'Quoted';
    private static $IN_PROGRESS_STRING = 'In progress';
    private static $CANCELLED_STRING   = 'Cancelled';
    private static $FINISHED_STRING    = 'Finished';

    /**
     * Test the model's constructor, equals and too string.
     */
    public function testConstructorToStringEquals()
    {
        $createdProjectStatus    = new ProjectStatus(self::$CREATED);
        $pendingProjectStatus    = new ProjectStatus(self::$PENDING);
        $quotedProjectStatus     = new ProjectStatus(self::$QUOTED);
        $inProgressProjectStatus = new ProjectStatus(self::$IN_PROGRESS);
        $cancelledProjectStatus  = new ProjectStatus(self::$CANCELLED);
        $finishedProjectStatus   = new ProjectStatus(self::$FINISHED);
        $unknownProjectStatus    = new ProjectStatus(self::$UNKNOWN);

        $this->assertTrue($createdProjectStatus->equals(new ProjectStatus(ProjectStatus::CREATED)));
        $this->assertTrue($createdProjectStatus->equals(ProjectStatus::CREATED));
        $this->assertEquals(self::$CREATED, $createdProjectStatus->getValue());
        $this->assertEquals((string) $createdProjectStatus, self::$CREATED_STRING);

        $this->assertTrue($pendingProjectStatus->equals(new ProjectStatus(ProjectStatus::PENDING)));
        $this->assertTrue($pendingProjectStatus->equals(ProjectStatus::PENDING));
        $this->assertEquals(self::$PENDING, $pendingProjectStatus->getValue());
        $this->assertEquals((string) $pendingProjectStatus, self::$PENDING_STRING);

        $this->assertTrue($quotedProjectStatus->equals(new ProjectStatus(ProjectStatus::QUOTED)));
        $this->assertTrue($quotedProjectStatus->equals(ProjectStatus::QUOTED));
        $this->assertEquals(self::$QUOTED, $quotedProjectStatus->getValue());
        $this->assertEquals((string) $quotedProjectStatus, self::$QUOTED_STRING);

        $this->assertTrue($inProgressProjectStatus->equals(new ProjectStatus(ProjectStatus::IN_PROGRESS)));
        $this->assertTrue($inProgressProjectStatus->equals(ProjectStatus::IN_PROGRESS));
        $this->assertEquals(self::$IN_PROGRESS, $inProgressProjectStatus->getValue());
        $this->assertEquals((string) $inProgressProjectStatus, self::$IN_PROGRESS_STRING);

        $this->assertTrue($cancelledProjectStatus->equals(new ProjectStatus(ProjectStatus::CANCELLED)));
        $this->assertTrue($cancelledProjectStatus->equals(ProjectStatus::CANCELLED));
        $this->assertEquals(self::$CANCELLED, $cancelledProjectStatus->getValue());
        $this->assertEquals((string) $cancelledProjectStatus, self::$CANCELLED_STRING);

        $this->assertTrue($finishedProjectStatus->equals(new ProjectStatus(ProjectStatus::FINISHED)));
        $this->assertTrue($finishedProjectStatus->equals(ProjectStatus::FINISHED));
        $this->assertEquals(self::$FINISHED, $finishedProjectStatus->getValue());
        $this->assertEquals((string) $finishedProjectStatus, self::$FINISHED_STRING);

        $this->assertEquals((string) $unknownProjectStatus, '');

        $this->assertFalse($createdProjectStatus->equals($quotedProjectStatus));
    }

    public function testCmp()
    {
        $createdProjectStatus = new ProjectStatus(self::$CREATED);
        $quotedProjectStatus  = new ProjectStatus(self::$QUOTED);

        $this->assertLessThan(0, $createdProjectStatus->cmp(ProjectStatus::QUOTED));
        $this->assertGreaterThan(0, $quotedProjectStatus->cmp(ProjectStatus::CREATED));
        $this->assertEquals(0, $createdProjectStatus->cmp(ProjectStatus::CREATED));
    }
}
