<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\JobStatus;

class JobStatusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $NEW         = 'NEW';
    private static $ANALYZING   = 'ANALYZING';
    private static $QUOTED      = 'QUOTED';
    private static $IN_PROGRESS = 'IN_PROGRESS';
    private static $TRANSLATED  = 'TRANSLATED';
    private static $CANCELLED   = 'CANCELLED';

    private static $NEW_STRING         = 'New';
    private static $ANALYZING_STRING   = 'Analyzing';
    private static $QUOTED_STRING      = 'Quoted';
    private static $IN_PROGRESS_STRING = 'In progress';
    private static $TRANSLATED_STRING  = 'Translated';
    private static $CANCELLED_STRING   = 'Cancelled';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorToStringEquals()
    {
        $newJobStatus        = new JobStatus(self::$NEW);
        $analyzingJobStatus  = new JobStatus(self::$ANALYZING);
        $quotedJobStatus     = new JobStatus(self::$QUOTED);
        $inProgressJobStatus = new JobStatus(self::$IN_PROGRESS);
        $translatedJobStatus = new JobStatus(self::$TRANSLATED);
        $cancelledJobStatus  = new JobStatus(self::$CANCELLED);

        $this->assertTrue($newJobStatus->equals(new JobStatus(self::$NEW)));
        $this->assertTrue($newJobStatus->equals(JobStatus::NEW_STATUS));
        $this->assertEquals(self::$NEW, $newJobStatus->getValue());
        $this->assertEquals((string) $newJobStatus, self::$NEW_STRING);

        $this->assertTrue($analyzingJobStatus->equals(new JobStatus(self::$ANALYZING)));
        $this->assertTrue($analyzingJobStatus->equals(JobStatus::ANALYZING));
        $this->assertEquals(self::$ANALYZING, $analyzingJobStatus->getValue());
        $this->assertEquals((string) $analyzingJobStatus, self::$ANALYZING_STRING);

        $this->assertTrue($quotedJobStatus->equals(new JobStatus(self::$QUOTED)));
        $this->assertTrue($quotedJobStatus->equals(JobStatus::QUOTED));
        $this->assertEquals(self::$QUOTED, $quotedJobStatus->getValue());
        $this->assertEquals((string) $quotedJobStatus, self::$QUOTED_STRING);

        $this->assertTrue($inProgressJobStatus->equals(new JobStatus(self::$IN_PROGRESS)));
        $this->assertTrue($inProgressJobStatus->equals(JobStatus::IN_PROGRESS));
        $this->assertEquals(self::$IN_PROGRESS, $inProgressJobStatus->getValue());
        $this->assertEquals((string) $inProgressJobStatus, self::$IN_PROGRESS_STRING);

        $this->assertTrue($translatedJobStatus->equals(new JobStatus(self::$TRANSLATED)));
        $this->assertTrue($translatedJobStatus->equals(JobStatus::TRANSLATED));
        $this->assertEquals(self::$TRANSLATED, $translatedJobStatus->getValue());
        $this->assertEquals((string) $translatedJobStatus, self::$TRANSLATED_STRING);

        $this->assertTrue($cancelledJobStatus->equals(new JobStatus(self::$CANCELLED)));
        $this->assertTrue($cancelledJobStatus->equals(JobStatus::CANCELLED));
        $this->assertEquals(self::$CANCELLED, $cancelledJobStatus->getValue());
        $this->assertEquals((string) $cancelledJobStatus, self::$CANCELLED_STRING);


        $this->assertFalse($newJobStatus->equals($analyzingJobStatus));
    }
}
