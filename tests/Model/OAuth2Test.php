<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\OAuth2;

/**
 * Test class for the MT API client.
 */
class OAuth2Test extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ACCESS_TOKEN  = 'accessToken';
    private static $REFRESH_TOKEN = 'refreshToken';
    private static $EXPIRES_IN    = 3600;
    private static $OAUTH2_JSON   = <<<END
{"access_token": "accessToken", "expires_in": 3600, "refresh_token": "refreshToken", "token_type": "Bearer"}
END;
    private static $OAUTH2_EXPIRED_JSON   = <<<END
{"access_token": "accessToken", "expires_in": 0, "refresh_token": "refreshToken", "token_type": "Bearer"}
END;

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorAndGetters()
    {
        $oauth2 = new OAuth2(json_decode(self::$OAUTH2_JSON));

        $this->assertEquals($oauth2->getAccessToken(), self::$ACCESS_TOKEN);
        $this->assertEquals($oauth2->getRefreshToken(), self::$REFRESH_TOKEN);
        $this->assertGreaterThan(time(), $oauth2->getExpiryDate());
        $this->assertLessThanOrEqual(time() + self::$EXPIRES_IN, $oauth2->getExpiryDate());
    }

    /**
     * Check the is valid check function.
     */
    public function testIsValid()
    {
        $oauth2 = new OAuth2(json_decode(self::$OAUTH2_JSON));

        $this->assertTrue($oauth2->isValid());

        $oauth2 = new OAuth2(json_decode(self::$OAUTH2_EXPIRED_JSON));

        $this->assertFalse($oauth2->isValid());
    }
}
