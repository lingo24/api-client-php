<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\Domain;

class DomainTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1        = 1;
    private static $NAME_1      = 'name1';
    private static $ID_2        = 2;
    private static $NAME_2      = 'name2';
    private static $DOMAIN_JSON = '{"id": 1, "name": "name1", "links": [{"rel": "self", "href": "domains/1"}]}';

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $domain = new Domain(json_decode(self::$DOMAIN_JSON));

        $this->assertEquals($domain->getId(), self::$ID_1);
        $this->assertEquals($domain->getName(), self::$NAME_1);
        $this->assertEquals($domain->getSelf(), Docs::$DOMAIN_ENDPOINT . '/' . self::$ID_1);

        $domain->setId(self::$ID_2);
        $domain->setName(self::$NAME_2);

        $this->assertEquals($domain->getId(), self::$ID_2);
        $this->assertEquals($domain->getName(), self::$NAME_2);
    }
}
