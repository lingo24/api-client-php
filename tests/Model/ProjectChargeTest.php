<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\ProjectCharge;

class ProjectChargeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $TITLE       = 'title';
    private static $VALUE       = 0.29;
    private static $CHARGE_JSON = <<<END
{
    "title": "title",
    "value": 0.29
}
END;

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $projectCharge = new ProjectCharge(json_decode(self::$CHARGE_JSON));

        $this->assertEquals($projectCharge->getTitle(), self::$TITLE);
        $this->assertEquals($projectCharge->getValue(), self::$VALUE);

        $projectCharge->setTitle(self::$TITLE);
        $projectCharge->setValue(self::$VALUE);
    }
}
