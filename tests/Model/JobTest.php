<?php

namespace Lingo24\API\Model;

use Lingo24\API\Docs;
use Lingo24\API\Model\Job;
use Lingo24\API\Model\JobStatus;

class JobTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $ID_1               = 1;
    private static $STATUS_1           = 'NEW';
    private static $SERVICE_ID_1       = 2;
    private static $SERVICE_1          = 'service';
    private static $SOURCE_LOCALE_ID_1 = 3;
    private static $SOURCE_LOCALE_1    = 'sourceLocale';
    private static $TARGET_LOCALE_ID_1 = 4;
    private static $TARGET_LOCALE_1    = 'targetLocale';
    private static $SOURCE_FILE_ID_1   = 5;
    private static $TARGET_FILE_ID_1   = 6;
    private static $ID_2               = 2;
    private static $STATUS_2           = 'ANALYZING';
    private static $STATUS_3           = 'QUOTED';
    private static $JOB_JSON           = <<<END
{
    "id": 1,
    "jobStatus": "NEW",
    "serviceId": 2,
    "sourceLocaleId": 3,
    "targetLocaleId": 4,
    "sourceFileId": 5,
    "targetFileId": 6,
    "links": [
        {"rel": "self", "href": "jobs/1"},
        {"rel": "service", "href": "services/2"},
        {"rel": "source-locale", "href": "locales/3"},
        {"rel": "target-locale", "href": "locales/4"},
        {"rel": "source-file", "href": "files/5"},
        {"rel": "target-file", "href": "files/6"},
        {"rel": "metrics", "href": "jobs/1/metrics"}
    ]
}
END;
    private static $JOB_PROJECT_JSON   = <<<END
{
    "id": 1,
    "jobStatus": "NEW",
    "serviceId": 2,
    "sourceLocaleId": 3,
    "targetLocaleId": 4,
    "sourceFileId": 5,
    "targetFileId": 6,
    "projectId": 10,
    "links": [{"rel": "project", "href": "projects/10"}]
}
END;
    private static $PROJECT_ID         = 10;
    private static $PROJECT_JSON       = <<<END
{
    "id": 10,
    "name": "projectName",
    "created": 1,
    "projectStatus": "CREATED",
    "links": [{"rel": "self", "href": "projects/10"}]
}
END;

    /**
     * Test the constructor based on API responses.
     */
    public function testObjectConstructorStandardGettersAndSetters()
    {
        $project = \Mockery::mock('Lingo24\API\Model\Project');
        $service = \Mockery::mock('Lingo24\API\Model\Service');
        $locale  = \Mockery::mock('Lingo24\API\Model\TranslationLocale');
        $file    = \Mockery::mock('Lingo24\API\Model\File');

        $job = new Job(json_decode(self::$JOB_JSON));

        $this->assertEquals($job->getId(), self::$ID_1);
        $this->assertEquals($job->getStatus(), new JobStatus(self::$STATUS_1));
        $this->assertEquals($job->getSelf(), Docs::$JOB_ENDPOINT . '/' . self::$ID_1);
        $this->assertEquals(
            $job->getLink(Job::METRICS_REL),
            Docs::$JOB_ENDPOINT . '/' . self::$ID_1 . '/' . Docs::$JOB_METRICS_ENDPOINT
        );
        $this->assertEquals($job->getLink(Job::SERVICE_REL), Docs::$SERVICE_ENDPOINT . '/' . self::$SERVICE_ID_1);
        $this->assertEquals(
            $job->getLink(Job::SOURCE_LOCALE_REL),
            Docs::$LOCALE_ENDPOINT . '/' . self::$SOURCE_LOCALE_ID_1
        );
        $this->assertEquals(
            $job->getLink(Job::TARGET_LOCALE_REL),
            Docs::$LOCALE_ENDPOINT . '/' . self::$TARGET_LOCALE_ID_1
        );
        $this->assertEquals($job->getLink(Job::SOURCE_FILE_REL), Docs::$FILE_ENDPOINT . '/' . self::$SOURCE_FILE_ID_1);
        $this->assertEquals($job->getLink(Job::TARGET_FILE_REL), Docs::$FILE_ENDPOINT . '/' . self::$TARGET_FILE_ID_1);

        $this->assertNull($job->getProject());
        $this->assertNull($job->getSourceLocale());
        $this->assertNull($job->getTargetLocale());
        $this->assertNull($job->getSourceFile());
        $this->assertNull($job->getTargetFile());

        $job->setId(self::$ID_2);
        $job->setStatus(new JobStatus(self::$STATUS_2));
        $job->setProject($project);
        $job->setService($service);
        $job->setSourceLocale($locale);
        $job->setTargetLocale($locale);
        $job->setSourceFile($file);
        $job->setTargetFile($file);

        $this->assertEquals($job->getId(), self::$ID_2);
        $this->assertEquals($job->getStatus(), new JobStatus(self::$STATUS_2));
        $this->assertEquals($job->getProject(), $project);
        $this->assertEquals($job->getService(), $service);
        $this->assertEquals($job->getSourceLocale(), $locale);
        $this->assertEquals($job->getTargetLocale(), $locale);
        $this->assertEquals($job->getSourceFile(), $file);
        $this->assertEquals($job->getTargetFile(), $file);

        $job->setStatus(self::$STATUS_3);

        $this->assertEquals($job->getStatus(), new JobStatus(self::$STATUS_3));
    }

    /**
     * Test the constructor based on a project object.
     */
    public function testProjectConstructor()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $this->assertEquals($job->getLink(Job::PROJECT_REL), Docs::$PROJECT_ENDPOINT . '/' . self::$PROJECT_ID);
    }

    /**
     * Test the project getter when using the href to fetch from the API.
     */
    public function testProjectDocsGetter()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job(json_decode(self::$JOB_PROJECT_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($project);

        $this->assertEquals($job->getProject($docs), $project);
    }

    /**
     * Test the docs project getter when there's no link.
     */
    public function testProjectDocsGetterNoLink()
    {
        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getProject($docs));
    }

    /**
     * Test the service getter when using the href to fetch from the API.
     */
    public function testServiceDocsGetter()
    {
        $service = \Mockery::mock('Lingo24\API\Model\Service');

        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($service);

        $this->assertEquals($job->getService($docs), $service);
    }

    /**
     * Test the docs service getter when there's no link.
     */
    public function testServiceDocsGetterNoLink()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getService($docs));
    }

    /**
     * Test the source locale getter when using the href to fetch from the API.
     */
    public function testSourceLocaleDocsGetter()
    {
        $locale = \Mockery::mock('Lingo24\API\Model\TranslationLocale');

        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($locale);

        $this->assertEquals($job->getSourceLocale($docs), $locale);
    }

    /**
     * Test the docs source locale getter when there's no link.
     */
    public function testSourceLocaleDocsGetterNoLink()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getSourceLocale($docs));
    }

    /**
     * Test the target locale getter when using the href to fetch from the API.
     */
    public function testTargetLocaleDocsGetter()
    {
        $locale = \Mockery::mock('Lingo24\API\Model\TranslationLocale');

        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($locale);

        $this->assertEquals($job->getTargetLocale($docs), $locale);
    }

    /**
     * Test the docs target locale getter when there's no link.
     */
    public function testTargetLocaleDocsGetterNoLink()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getTargetLocale($docs));
    }

    /**
     * Test the source file getter when using the href to fetch from the API.
     */
    public function testSourceFileDocsGetter()
    {
        $file = \Mockery::mock('Lingo24\API\Model\File');

        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($file);

        $this->assertEquals($job->getSourceFile($docs), $file);
    }

    /**
     * Test the docs source fuke getter when there's no link.
     */
    public function testSourceFileGetterNoLink()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getSourceFile($docs));
    }

    /**
     * Test the target file getter when using the href to fetch from the API.
     */
    public function testTargetFileDocsGetter()
    {
        $file = \Mockery::mock('Lingo24\API\Model\File');

        $job = new Job(json_decode(self::$JOB_JSON));

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $docs
            ->shouldReceive('getResource')
            ->andReturn($file);

        $this->assertEquals($job->getTargetFile($docs), $file);
    }

    /**
     * Test the docs target file getter when there's no link.
     */
    public function testTargetFileDocsGetterNoLink()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $job = new Job($project);

        $docs = \Mockery::mock('Lingo24\API\Docs');

        $this->assertNull($job->getTargetFile($docs));
    }

    /**
     * Test that the getFileTypeRel() method returns the correct relationships.
     */
    public function testGetFileTypeRel()
    {
        $this->assertEquals(Job::getFileTypeRel(FileType::SOURCE), Job::SOURCE_FILE_REL);
        $this->assertEquals(Job::getFileTypeRel(FileType::TARGET), Job::TARGET_FILE_REL);
        $this->assertEquals(Job::getFileTypeRel(null), null);
    }
}
