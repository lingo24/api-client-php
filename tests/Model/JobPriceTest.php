<?php

namespace Lingo24\API\Model;

use Lingo24\API\Model\JobPrice;

class JobPriceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $CURRENCY                 = 'GBP';
    private static $TOTAL_WO_VAT_W_DISCOUNT  = 0.29;
    private static $TOTAL_WO_VAT_WO_DISCOUNT = 0.29;
    private static $TOTAL_W_VAT_W_DISCOUNT   = 0.35;
    private static $TOTAL_W_VAT_WO_DISCOUNT  = 0.35;
    private static $PRICE_JSON               = <<<END
{
    "currencyCode": "GBP",
    "totalWoVatWDiscount": 0.29,
    "totalWoVatWoDiscount": 0.29,
    "totalWVatWDiscount": 0.35,
    "totalWVatWoDiscount": 0.35
}
END;

    /**
     * Test the model's constructor, getters and setters.
     */
    public function testConstructorGettersAndSetters()
    {
        $jobPrice = new JobPrice(json_decode(self::$PRICE_JSON));

        $this->assertEquals($jobPrice->getCurrency(), self::$CURRENCY);
        $this->assertEquals($jobPrice->getTotalWoVatWDiscount(), self::$TOTAL_WO_VAT_W_DISCOUNT);
        $this->assertEquals($jobPrice->getTotalWoVatWoDiscount(), self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $this->assertEquals($jobPrice->getTotalWVatWDiscount(), self::$TOTAL_W_VAT_W_DISCOUNT);
        $this->assertEquals($jobPrice->getTotalWVatWoDiscount(), self::$TOTAL_W_VAT_WO_DISCOUNT);

        $jobPrice->setCurrency(self::$CURRENCY);
        $jobPrice->setTotalWoVatWDiscount(self::$TOTAL_WO_VAT_W_DISCOUNT);
        $jobPrice->setTotalWoVatWoDiscount(self::$TOTAL_WO_VAT_WO_DISCOUNT);
        $jobPrice->setTotalWVatWDiscount(self::$TOTAL_W_VAT_W_DISCOUNT);
        $jobPrice->setTotalWVatWoDiscount(self::$TOTAL_W_VAT_WO_DISCOUNT);
    }
}
