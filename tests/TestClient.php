<?php

namespace Lingo24\API;

/**
 * Implementation of the abstract client class to use in tests.
 */
class TestClient extends AbstractClient
{
}
