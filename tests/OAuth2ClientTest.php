<?php

namespace Lingo24\API;

use Lingo24\API\Model\OAuth2;
use Lingo24\API\Model\Project;
use Lingo24\API\Exception\Lingo24ApiAuthorisationException;

/**
 * Test class for the API OAuth2 client function.
 */
class OAuth2ClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test data.
     */
    private static $URL           = 'http://localhost';
    private static $CLIENT_ID     = 'clientId';
    private static $CLIENT_SECRET = 'clientSecret';
    private static $REDIRECT_URI  = 'http://localhost';
    private static $AUTH_CODE     = 'authCode';
    private static $OAUTH2_JSON   =
         '{"access_token": "accessToken", "expires_in": 3600, "refresh_token": "refreshToken", "token_type": "Bearer"}';
    private static $OAUTH2_EXPIRED_JSON =
         '{"access_token": "accessToken", "expires_in": 0, "refresh_token": "refreshToken", "token_type": "Bearer"}';
    private static $PROJECT_JSON  = '{"id": 1, "name": "name1", "created": 1, "projectStatus": "CREATED"}';
    private static $BASE_URL      = 'http://localhost/base';

    /**
     * @var Client Instance of the Client class to use in tests.
     */
    private $client;

    /**
     * Instantiates a mock of the TestClient class to test the OAuth2 client abstract class.
     *
     * @see PHPUnit_Framework_TestCase::setUp()
     */
    public function setUp()
    {
        $this->oauth2 = new OAuth2(json_decode(self::$OAUTH2_JSON));
        $this->expiredOAuth2 = new OAuth2(json_decode(self::$OAUTH2_EXPIRED_JSON));


        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI, $this->oauth2)
        );
    }

    /**
     * Tests the simple constructor case containing just the client API data.
     */
    public function testConstructor()
    {
        $client = new TestOAuth2Client(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI);

        $this->assertNotNull($client);
        $this->assertEquals($client->getBaseUrl(), TestOAuth2Client::$BASE_URL_LIVE);
    }

    /**
     * Test the constructor throws an exception is the client data is missing.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testConstructorErrorOne()
    {
        $client = new TestOAuth2Client(null, null, null);
    }

    /**
     * Test the constructor throws an exception is the client data is missing.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testConstructorErrorTwo()
    {
        $client = new TestOAuth2Client(self::$CLIENT_ID, null, null);
    }

    /**
     * Test the constructor throws an exception is the client data is missing.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testConstructorErrorThree()
    {
        $client = new TestOAuth2Client(self::$CLIENT_ID, self::$CLIENT_SECRET, null);
    }

    /**
     * Tests the constructor with an environment option passed.
     */
    public function testConstructorDemoEnv()
    {
        $client = new TestOAuth2Client(
            self::$CLIENT_ID,
            self::$CLIENT_SECRET,
            self::$REDIRECT_URI,
            null,
            TestOAuth2Client::$DEMO_ENV
        );

        $this->assertEquals($client->getEnv(), TestOAuth2Client::$DEMO_ENV);
        $this->assertEquals($client->getBaseUrl(), TestOAuth2Client::$BASE_URL_DEMO);
    }

    /**
     * Tests the constructor with a custom base URL set.
     */
    public function testConstructorBaseUrl()
    {
        $client = new TestOAuth2Client(
            self::$CLIENT_ID,
            self::$CLIENT_SECRET,
            self::$REDIRECT_URI,
            null,
            TestOAuth2Client::$DEMO_ENV,
            self::$BASE_URL
        );

        $this->assertEquals($client->getEnv(), TestOAuth2Client::$DEMO_ENV);
        $this->assertEquals(self::$BASE_URL, $client->getCustomBaseUrl());
        $this->assertEquals(self::$BASE_URL, $client->getBaseUrl());
    }

    /**
     * Test the get function.
     */
    public function testGet()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->getJson(self::$URL);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the get function when the API returns an authorisation error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testGetAuthorisationError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '401')));

        $this->client->get(self::$URL);
    }

    /**
     * Test the get function when the API returns a server error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testGetServerError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '500')));

        $this->client->get(self::$URL);
    }

    /**
     * Test the get function when the API returns a not found.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiNotFoundException
     */
    public function testGetNotFoundError()
    {
        $this->client
        ->shouldReceive('curl')
        ->andReturn(array('{"foo": "bar"}', array('http_code' => '404')));

        $this->client->get(self::$URL);
    }

    /**
     * Test the get function with no authorisation required.
     */
    public function testGetNoAuth()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->getJson(self::$URL, array(), true);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the get function when there is an authorisation failure.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testGetAuthorisationRefreshFailure()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl,post]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client->oauth2 = $this->expiredOAuth2;

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client
            ->shouldReceive('post')
            ->andThrow(new Lingo24ApiAuthorisationException());

        $this->client->get(self::$URL);
    }

    /**
     * Test the get function when the client has not yet been authorised.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testGetNotAuthenticated()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client->get(self::$URL);
    }

    /**
     * Test the post function.
     */
    public function testPost()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->post(self::$URL);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the post function for multipart requests.
     */
    public function testPostMultipart()
    {
        $params = array('foo' => array('contentType' => 'application/json', 'body' => '{"foo": "bar"}'));
        $options = array('contentType' => 'multipart/form-data');

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->post(self::$URL, $params, false, $options);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the post function when the API returns an authorisation error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPostAuthorisationError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '401')));

        $this->client->post(self::$URL);
    }

    /**
     * Test the post function when the API returns a server error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testPostServerError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '500')));

        $this->client->post(self::$URL);
    }

    /**
     * Test the post function with no authorisation required.
     */
    public function testPostNoAuth()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->post(self::$URL, array(), true);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the post function when there is an authorisation failure.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPostAuthError()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl,post]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI, $this->oauth2)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client
            ->shouldReceive('post')
            ->with('http://localhost')
            ->andThrow(new Lingo24ApiAuthorisationException());

        $this->client->post(self::$URL);
    }

    /**
     * Test the post json function when the client has not yet been authorised.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPostNotAuthenticated()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client->post(self::$URL);
    }

    /**
     * Test the post json function.
     */
    public function testPostJson()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->postJson(self::$URL, self::$PROJECT_JSON);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the post json function when the API returns an authorisation error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPostJsonAuthorisationError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '401')));

        $this->client->postJson(self::$URL, self::$PROJECT_JSON);
    }

    /**
     * Test the post json function when the API returns a server error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testPostJsonServerError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '500')));

        $this->client->postJson(self::$URL, self::$PROJECT_JSON);
    }

    /**
     * Test the post json function when the client has not yet been authorised.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPostJsonNotAuthenticated()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client->postJson(self::$URL, self::$PROJECT_JSON);
    }

    /**
     * Test the put function.
     */
    public function testPut()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->putJson(self::$URL, array());

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the put function when the API returns an authorisation error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPutAuthorisationError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '401')));

        $this->client->putJson(self::$URL, array());
    }

    /**
     * Test the put function when the API returns a server error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testPutServerError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '500')));

        $this->client->putJson(self::$URL, array());
    }

    /**
     * Test the put function with no authorisation required.
     */
    public function testPutNoAuth()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $response = $this->client->putJson(self::$URL, array(), true);

        $this->assertEquals("bar", $response->foo);
    }

    /**
     * Test the put function when the client has not yet been authorised.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testPutNotAuthenticated()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"foo": "bar"}', array('http_code' => '200')));

        $this->client->putJson(self::$URL, array());
    }

    /**
     * Test the delete function.
     */
    public function testDelete()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '204')));

        $this->client->delete(self::$URL);
    }

    /**
     * Test the delete function when the API returns an authorisation error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testDeleteAuthorisationError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '401')));

        $this->client->delete(self::$URL);
    }

    /**
     * Test the delete function when the API returns an not found error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiNotFoundException
     */
    public function testDeleteNotFoundError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '404')));

        $this->client->delete(self::$URL);
    }

    /**
     * Test the delete function when the API returns an not allowed error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiNotAllowedException
     */
    public function testDeleteNotAllowedError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '405')));

        $this->client->delete(self::$URL);
    }

    /**
     * Test the delete function when the API returns a server error.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiException
     */
    public function testDeleteServerError()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '500')));

        $this->client->delete(self::$URL);
    }

    /**
     * Test the delete function when the client has not yet been authorised.
     *
     * @expectedException Lingo24\API\Exception\Lingo24ApiAuthorisationException
     */
    public function testDeleteNotAuthenticated()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '200')));

        $this->client->delete(self::$URL);
    }

    /**
     * Check the authorise function, with a valid OAuth2 object.
     */
    public function testAuthorise()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $result = $this->client->authorise($this->oauth2);

        $this->assertTrue($result);
    }

    /**
     * Check the authorise function, with an invalid OAuth2 object.
     */
    public function testAuthoriseInvalid()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array(self::$OAUTH2_JSON, array('http_code' => '200')));

        $result = $this->client->authorise($this->expiredOAuth2);

        $this->assertTrue($result);
        $this->assertTrue($this->client->getOAuth2()->isValid());
    }

    /**
     * Check the authorise function, with an invalid OAuth2 object, where the refresh returns an error.
     */
    public function testAuthoriseInvalidError()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andThrow(new Lingo24ApiAuthorisationException());

        $result = $this->client->authorise($this->expiredOAuth2);

        $this->assertFalse($result);
    }

    /**
     * Check the authorise function, with an invalid OAuth2 object. Uses the authorize() alias for coverage.
     */
    public function testAuthoriseAuthCode()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array(self::$OAUTH2_JSON, array('http_code' => '200')));

        $result = $this->client->authorize(self::$AUTH_CODE);

        $this->assertTrue($result);
        $this->assertTrue($this->client->getOAuth2()->isValid());
    }

    /**
     * Check the authorise function, with an invalid OAuth2 object, where the refresh returns an error. Uses the
     * authorize() alias for coverage.
     */
    public function testAuthoriseAuthCodeError()
    {
        $this->client = \Mockery::mock(
            'Lingo24\API\TestOAuth2Client[curl]',
            array(self::$CLIENT_ID, self::$CLIENT_SECRET, self::$REDIRECT_URI)
        );

        $this->client
            ->shouldReceive('curl')
            ->andThrow(new Lingo24ApiAuthorisationException());

        $result = $this->client->authorize(self::$AUTH_CODE);

        $this->assertFalse($result);
    }

    /**
     * Test the getResource() method, which provides a generic way to fetch resources using their URL.
     */
    public function testGetResource()
    {
        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('{"id": 1, "name": "domainName"}', array('http_code' => '200')));

        $response = $this->client->getResource(self::$URL, '\Lingo24\API\Model\Domain');

        $this->assertEquals(1, $response->getId());
        $this->assertEquals("domainName", $response->getName());
    }

    /**
     * Test the deleteResource() method, which provides a generic way to delete resources.
     */
    public function testDeleteResource()
    {
        $project = new Project(json_decode(self::$PROJECT_JSON));

        $this->client
            ->shouldReceive('curl')
            ->andReturn(array('', array('http_code' => '204')));

        $response = $this->client->deleteResource($project);
    }
}
